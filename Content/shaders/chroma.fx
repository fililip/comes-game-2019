﻿float2 resolution;
Texture2D tex;
float m;
float time;

SamplerState state
{
	AddressU = CLAMP;
	AddressV = CLAMP;
	MagFilter = POINT;
	MinFilter = POINT;
	Mipfilter = POINT;
};

struct VertexShaderOutput
{
    float4 Position : SV_POSITION;
    float4 Color : COLOR0;
    float2 TexCoords : TEXCOORD0;
};

float rand(float n)
{
    return frac(sin(n) * 43758.5453123);
}

float noise(float p)
{
    float fl = floor(p);
    float fc = frac(p);
    return lerp(rand(fl), rand(fl + 1.0), fc);
}

float gaussian(float x, float p, float w, float a)
{
    return -a * exp( -( pow(x - p, 2.0) / pow(w, 2.0) ) ) + a;
}

float dist(float from, float current, float multiplier)
{
    if (current < from)
    	return distance(from, current) * gaussian(current, from, from * m, multiplier);
    else
        return distance(from, current) * gaussian(current, from, from * m, multiplier) * -1.0;
}

float4 MainPS(VertexShaderOutput input) : COLOR
{
	float2 pixel = input.TexCoords * resolution;

	float2 uv = float2(pixel.x, pixel.y);
    
    //uv.x += noise(m + (pixel.y / 8.0 + time * 420.0) * 2.0) * 3.0;
    //uv.y += noise(m + (pixel.y / 8.0 + time * 420.0) * 2.0) * 3.0;
    
	uv.x += dist(resolution.x / 2.0, pixel.x, 0.0295 * m);
    uv.y += dist(resolution.y / 2.0, pixel.y, 0.0295 * m);

    float2 r_uv = uv, g_uv = uv, b_uv = uv;

    r_uv.x += dist(resolution.x / 2.0, pixel.x, 0.005 * m);
    r_uv.y += dist(resolution.y / 2.0, pixel.y, 0.005 * m);
    
    g_uv.x += dist(resolution.x / 2.0, pixel.x, 0.01 * m);
    g_uv.y += dist(resolution.y / 2.0, pixel.y, 0.01 * m);
    
    b_uv.x += dist(resolution.x / 2.0, pixel.x, 0.015 * m);
    b_uv.y += dist(resolution.y / 2.0, pixel.y, 0.015 * m);
    
    r_uv /= resolution.xy;
    g_uv /= resolution.xy;
    b_uv /= resolution.xy;

    uv /= resolution.xy;
    
    float4 c = float4(1.0, 1.0, 1.0, 1.0);
    
    c.r = tex.Sample(state, r_uv).r;
    c.g = tex.Sample(state, g_uv).g;
    c.b = tex.Sample(state, b_uv).b;
    
    c.a = tex.Sample(state, b_uv).a * smoothstep(0.0, 1.0, 1.0 - (distance(pixel.xy, resolution.xy / 2) / (resolution.xy)));
    
    return c;
}

technique BasicColorDrawing
{
	pass P0
	{
		PixelShader = compile ps_3_0 MainPS();
	}
};