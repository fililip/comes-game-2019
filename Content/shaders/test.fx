﻿Texture2D tex;
float time;
matrix mat;

SamplerState state
{
	Filter = Linear;
	AddressU = Wrap;
	AddressV = Wrap;
};

struct VertexShaderInput
{
	float4 Position : POSITION0;
	float4 Color : COLOR0;
	float2 TexCoords: TEXCOORD0;
};

struct VertexShaderOutput
{
	float4 Position : SV_POSITION;
	float4 Color : COLOR0;
	float2 TexCoords: TEXCOORD0;
};

float rand(float n)
{
	return frac(sin(n) * 43758.5453123);
}

float noise(float p)
{
	float fl = floor(p);
    float fc = frac(p);
	return lerp(rand(fl), rand(fl + 1.0), fc);
}

VertexShaderOutput MainVS(in VertexShaderInput input)
{
	VertexShaderOutput output = (VertexShaderOutput)0;

	output.Position = mul(input.Position, mat);
	output.TexCoords = input.TexCoords;
	output.Color = input.Color;

	return output;
}

float4 MainPS(VertexShaderOutput input) : COLOR
{
    return tex.Sample(state, float2(input.TexCoords.x + (noise(time * input.TexCoords.x) * 0.01), input.TexCoords.y + (noise(time * input.TexCoords.y) * 0.01)));
}

technique BasicColorDrawing
{
	pass P0
	{
		VertexShader = compile vs_3_0 MainVS();
		PixelShader = compile ps_3_0 MainPS();
	}
};