﻿float2 resolution;
Texture2D tex;
int horizontal;

SamplerState state
{
	AddressU = CLAMP;
	AddressV = CLAMP;
	MagFilter = POINT;
	MinFilter = POINT;
	Mipfilter = POINT;
};

struct VertexShaderOutput
{
    float4 Position : SV_POSITION;
    float4 Color : COLOR0;
    float2 TexCoords : TEXCOORD0;
};

float4 PS(VertexShaderOutput input) : COLOR
{
	float2 pixel = input.TexCoords * resolution;

	float4 color = float4(0, 0, 0, 1);

	float dist = clamp(distance(pixel, resolution / 2.0) / 950.0, 0.0, 1.0);

	for (int i = -7; i < 7; i++)
	{
		float kernel = (float)(7 - abs(i)) / 48.0;

		if (horizontal == 1)
			color += tex.Sample(state, float2(pixel.x + 1.0 * (float)i * dist, pixel.y) / resolution) * kernel;
		else
			color += tex.Sample(state, float2(pixel.x, pixel.y + 1.0 * (float)i * dist) / resolution) * kernel;
	}

	return color;
}

technique BasicColorDrawing
{
	pass P0
	{
		PixelShader = compile ps_3_0 PS();
	}
};