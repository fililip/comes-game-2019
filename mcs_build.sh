#!/usr/bin/bash

mkdir LINUX
mcs src/*.cs -o+ -langversion:7.2 -out:LINUX/comes -r:deps/MonoGame.Framework.dll -r:deps/DiscordRPC.dll
mkdir LINUX/content
mkdir LINUX/content/audio
mkdir LINUX/content/maps
cp -r Content/bin/DesktopGL/Content/. LINUX/content
cp -r Content/audio/. LINUX/content/audio
cp -r Content/maps/. LINUX/content/maps
cp deps/libfmod.so LINUX
cp deps/MonoGame.Framework.dll LINUX
cp deps/DiscordRPC.dll LINUX
cp deps/Newtonsoft.Json.dll LINUX
cd LINUX/
mono comes