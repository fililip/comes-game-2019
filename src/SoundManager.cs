﻿using System;
using System.Collections.Generic;
//using System.IO;
//using System.Runtime.InteropServices;

namespace Comes
{
    class Sound
    {
        public FMOD.Sound sound;
        public FMOD.Channel channel;

        public uint pauseTime = 0;
        public float volume = 1;
    }

    class SoundManager
    {
        private static FMOD.System soundSystem;
        private static FMOD.ChannelGroup channelGroup = new FMOD.ChannelGroup();
        private static List<Sound> sounds = new List<Sound>();

        public static void Initialize()
        {
            FMOD.Factory.System_Create(out soundSystem);
            soundSystem.init(32, FMOD.INITFLAGS.NORMAL, IntPtr.Zero);
        }

        public static Sound LoadSound(string filename, bool loop = false)
        {
            FMOD.Sound sound;

            FMOD.RESULT soundCreationResult = soundSystem.createSound("content/audio/" + filename, FMOD.MODE.DEFAULT, out sound);

            if (soundCreationResult != FMOD.RESULT.OK)
            {
                ComesGame.ShowError("reading audio file '" + filename + "' failed", true);
                return null;
            }

            if (loop)
                sound.setMode(FMOD.MODE.LOOP_NORMAL);
            else
                sound.setMode(FMOD.MODE.LOOP_OFF);

            Sound s = new Sound();
            s.sound = sound;
            s.channel = default;

            sounds.Add(s);

            return s;
        }

        public static void AddDSP(Sound sound, int index, FMOD.DSP_TYPE type)
        {
            FMOD.DSP dsp;
            soundSystem.createDSPByType(type, out dsp);
            sound.channel.addDSP(index, dsp);
        }

        private static void OnError(string name)
        {
            ComesGame.ShowError("sound " + name + " not found");
        }

        public FMOD.DSP GetDSP(Sound sound, int index)
        {
            FMOD.DSP dsp;
            sound.channel.getDSP(index, out dsp);

            return dsp;
        }

        public static void SetDSPParameter(Sound sound, int dspIndex, int paramIndex, float value)
        {
            FMOD.DSP dsp;
            sound.channel.getDSP(dspIndex, out dsp);

            dsp.setParameterFloat(paramIndex, value);
        }

        public static float GetDSPParameter(Sound sound, int dspIndex, int paramIndex)
        {
            FMOD.DSP dsp;
            sound.channel.getDSP(dspIndex, out dsp);

            float value;
            dsp.getParameterFloat(paramIndex, out value);

            return value;
        }

        public static int GetDSPCount(Sound sound)
        {
            int count;
            sound.channel.getNumDSPs(out count);

            return count;
        }

        public static void SetLoop(Sound sound, bool loop = true)
        {
            if (loop)
                sound.sound.setMode(FMOD.MODE.LOOP_NORMAL);
            else
                sound.sound.setMode(FMOD.MODE.LOOP_OFF);
        }

        public static void StopAllSounds()
        {
            foreach (Sound sound in sounds)
            {
                if (!EqualityComparer<FMOD.Channel>.Default.Equals(sound.channel, default))
                    sound.channel.stop();
            }
        }

        public static void StopSound(Sound sound)
        {
            if (!EqualityComparer<FMOD.Channel>.Default.Equals(sound.channel, default))
                sound.channel.stop();
        }

        public static void PlaySound(Sound sound)
        {
            if (!EqualityComparer<FMOD.Channel>.Default.Equals(sound.channel, default))
                sound.channel.stop();

            FMOD.Channel channel;

            soundSystem.playSound(sound.sound, channelGroup, false, out channel);

            sound.channel = channel;
        }

        public static void PauseSound(Sound sound)
        {
            uint position;
            sound.channel.getPosition(out position, FMOD.TIMEUNIT.MS);

            sound.pauseTime = position;
            sound.channel.setVolume(0);
        }

        public static void ResumeSound(Sound sound)
        {
            sound.channel.setPosition(sound.pauseTime, FMOD.TIMEUNIT.MS);
            sound.channel.setVolume(sound.volume);
        }

        public static void SetVolume(Sound sound, float value)
        {
            sound.volume = value;
            sound.channel.setVolume(value);
        }

        public static float GetVolume(Sound sound)
        {
            float value;
            sound.channel.getVolume(out value);

            return value;
        }

        public static FMOD.Channel GetChannel(Sound sound)
        {
            return sound.channel;
        }

        public static void Update()
        {
            soundSystem.update();
        }

        /*
        private static byte[] ReadStream(Stream input)
        {
            try
            {
                byte[] buffer = new byte[input.Length];

                MemoryStream ms = new MemoryStream();

                int read;

                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }

                return ms.ToArray();
            } catch
            {
                return null;
            }
        }

        public static void LoadSound(string name, bool loop = false)
        {
            FMOD.Sound sound;

#if NEVER_OCCURS
            byte[] audioBuffer = ReadStream(Properties.Resources.ResourceManager.GetStream(name));

            if (audioBuffer == null)
            {
                ComesGame.ShowError("reading audio stream failed: incorrect parameter '" + name + "'", true);
                return;
            }

            FMOD.CREATESOUNDEXINFO info = new FMOD.CREATESOUNDEXINFO();
            info.cbsize = Marshal.SizeOf(info);
            info.length = (uint)audioBuffer.Length;

            soundSystem.createSound(audioBuffer, FMOD.MODE.OPENMEMORY_POINT | FMOD.MODE.CREATESTREAM, ref info, out sound);
#elif SAME_HERE
            soundSystem.createSound(name, FMOD.MODE.DEFAULT, out sound);
#endif
            FMOD.RESULT soundCreationResult = soundSystem.createSound("content/audio/" + name, FMOD.MODE.DEFAULT, out sound);

            if (soundCreationResult != FMOD.RESULT.OK)
            {
                ComesGame.ShowError("reading audio file '" + name + "' failed", true);
                return;
            }

            if (loop)
                sound.setMode(FMOD.MODE.LOOP_NORMAL);
            else
                sound.setMode(FMOD.MODE.LOOP_OFF);

            Sound s = new Sound();
            s.sound = sound;
            s.channel = default;

            sounds.Add(name, s);
        }
        */
    }
}