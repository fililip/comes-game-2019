﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Comes
{
    class TextCharacter
    {
        public char character = '\0';
        public Color color = Color.White;
        public bool shake = false;
    }

    class Text
    {
        public List<TextCharacter> characters = new List<TextCharacter>();
        public int timeout = 400;

        public string GetString()
        {
            string text = "";

            foreach (TextCharacter c in characters)
                text += c.character;

            return text;
        }
    }

    class TextBlitter
    {
        private SpriteFont font;

        private List<Text> list = new List<Text>();
        private int index = 0;
        private int substringIndex = 0;
        private Stopwatch stopwatch = new Stopwatch();
        private int timeout = 0;
        private bool active = false;
        public bool finished { get; private set; } = false;
        private Random random = new Random();
        public Vector2 position = new Vector2(0, 0);
        private GameTime gameTime = new GameTime();
        private bool fadeIn = true;
        private float opacity = 1f;

        public TextBlitter(ref SpriteFont font)
        {
            this.font = font;
        }

        public void SetActive(bool active = true)
        {
            stopwatch.Reset();

            if (active)
                stopwatch.Start();

            this.active = active;
        }

        public void AddString(string s, int timeout = 400)
        {
            Text text = new Text();
            text.timeout = timeout;
            
            foreach (char c in s)
            {
                TextCharacter character = new TextCharacter();
                character.character = c;

                text.characters.Add(character);
            }

            list.Add(text);
        }

        public void AddText(Text text)
        {
            list.Add(text);
        }

        private Vector2 GetCharSize(char c)
        {
            return font.MeasureString(c.ToString());
        }

        public Vector2 GetStringSize()
        {
            return font.MeasureString(GetString());
        }

        private string GetString()
        {
            string s = "";

            if (list.Count > 0)
                s = list[index].GetString();

            return s;
        }

        public void Update(GameTime gameTime)
        {
            this.gameTime = gameTime;

            if (active && list.Count > 0)
            {
                fadeIn = true;
                opacity = 1f;

                if (stopwatch.ElapsedMilliseconds > timeout)
                {
                    if (substringIndex <= GetString().Length - 1)
                    {
                        TextCharacter character = list[index].characters[substringIndex];
                        char c = character.character;

                        switch (c)
                        {
                            case ' ':
                            case '\n':
                                substringIndex++;
                                return;
                            case ',':
                                timeout = 400;
                                break;
                            case '.':
                                //timeout = 700;
                                break;
                            default:
                                timeout = 50;
                                break;
                        }

                        substringIndex++;
                    }
                    else
                    {
                        stopwatch.Reset();
                        active = false;

                        new Task(() =>
                        {
                            Thread.Sleep(list[index].timeout);
                            fadeIn = false;
                            Thread.Sleep(400);

                            if (list.Count > 0)
                            {
                                substringIndex = 0;
                                list.RemoveAt(0);

                                if (list.Count == 0)
                                {
                                    finished = true;
                                    return;
                                }

                                active = true;
                                stopwatch.Start();
                            }
                        }).Start();
                    }

                    stopwatch.Restart();
                }
            }

            if (!fadeIn)
            {
                if (opacity > 0.1f)
                    opacity -= (float)gameTime.ElapsedGameTime.TotalSeconds * 8f;
                else
                    opacity = 0f;
            }
        }

        public void Draw(ref SpriteBatch spriteBatch)
        {
            if (list.Count > 0)
            {
                int x = (int)position.X;
                int y = (int)position.Y;

                for (int i = 0; i < substringIndex; i++)
                {
                    TextCharacter character = list[index].characters[i];
                    char c = character.character;

                    if (c == '\n')
                    {
                        x = (int)position.X;
                        y += (int)GetCharSize('1').Y;
                        continue;
                    }

                    string s = c.ToString();
                    Color color = character.color;

                    float offsetX = 0, offsetY = 0;

                    if (character.shake)
                    {
                        offsetX = random.Next(0, 5) - 2f * (float)gameTime.ElapsedGameTime.TotalSeconds;
                        offsetY = random.Next(0, 5) - 2f * (float)gameTime.ElapsedGameTime.TotalSeconds;
                    }

                    color.A = (byte)(opacity * 255f);

                    spriteBatch.DrawString(font, s, new Vector2(x + offsetX, y + offsetY), color);

                    x += (int)GetCharSize(c).X;
                }
            }
        }
    }
}