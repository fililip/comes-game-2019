﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Comes
{
    class OnScreenDisplay
    {
        private SpriteFont font;
        private Stopwatch stopwatch = new Stopwatch();

        private string text = "";
        private float opacity = 0f;
        private int padding = 48;
        private bool fadeIn = false;

        private int timeout = 4000;
        private float speed = 1;

        public OnScreenDisplay(ref SpriteFont font)
        {
            this.font = font;
        }

        public void ShowText(string text, int timeout = 4000, float speed = 1)
        {
            stopwatch.Reset();
            stopwatch.Start();
            fadeIn = true;
            this.text = text;
            this.timeout = timeout;
            this.speed = speed;
        }

        private Vector2 GetTextSize()
        {
            return font.MeasureString(text);
        }

        public void Update(GameTime gameTime)
        {
            if (speed < 0.1f)
                speed = 0.1f;

            if (stopwatch.ElapsedMilliseconds > timeout)
                fadeIn = false;

            if (fadeIn)
            {
                if (opacity < 0.75f)
                    opacity += (float)gameTime.ElapsedGameTime.TotalSeconds * speed;
                else
                    opacity = 0.75f;
            } else
            {
                if (opacity > 0)
                    opacity -= (float)gameTime.ElapsedGameTime.TotalSeconds * speed;
                else
                    opacity = 0f;
            }
        }

        public void Draw(ref SpriteBatch spriteBatch)
        {
            int width = (int)GetTextSize().X + padding;
            int height = (int)GetTextSize().Y + padding;

            spriteBatch.Draw(TextureManager.textures["white"],
                             new Rectangle(ComesGame.width / 2 - width / 2, ComesGame.height / 2 - height / 2, width, height),
                             new Color(0.1f, 0.1f, 0.1f, opacity));

            spriteBatch.DrawString(font, text, new Vector2(ComesGame.width / 2 - width / 2 + padding / 2,
                                                           ComesGame.height / 2 - height / 2 + padding / 2), new Color(1f, 1f, 1f, opacity));
        }
    }

    class GameMenuItem
    {
        public bool selected = false;
        public string text = "";
        public float _color = 0f;
        public float color = 0f;

        public GameMenuItem(string text = "", bool selected = false)
        {
            this.text = text;
            this.selected = selected;
        }
    }

    class GameMenu
    {
        private SpriteFont font;
        public List<GameMenuItem> menuItems = new List<GameMenuItem>();
        private GameTime gameTime = new GameTime();

        private Stopwatch blinkTimer = new Stopwatch();

        public bool focused { get; private set; } = false;

        private float _topPositionY = 0f;
        private float topPositionY = 0f;
        private float topDefaultPositionY = 0f;
        private float topFocusedPositionY = 0f;

        private float _bottomPositionY = 0f;
        private float bottomPositionY = 0f;
        private float bottomDefaultPositionY = 0f;
        private float bottomFocusedPositionY = 0f;

        private int padding = 18;

        private bool pUpPressed = false;
        private bool pDownPressed = false;
        private bool pLeftPressed = false;
        private bool pRightPressed = false;

        private bool pAccept = false;
        private bool pCancel = false;

        private int selectedItem = 0;

        private UI.Text optionsTitle;
        private List<UI.Text> options = new List<UI.Text>();
        private int selectedOption = 0;

        private void AddOption(string name, string text)
        {
            UI.Text option = new UI.Text(ref font);
            option.color = new Color(1f, 1f, 1f, 0f);
            option.text = text;
            option.SetAttribute("opacity", "0.0");
            option.SetAttribute("y", "0");
            option.SetAttribute("name", name);
            options.Add(option);
        }

        public GameMenu(ref SpriteFont font)
        {
            this.font = font;

            menuItems.Add(new GameMenuItem("game"));
            menuItems.Add(new GameMenuItem("inventory"));

            _topPositionY = topPositionY = topDefaultPositionY = -(GetTextSize("123").Y + padding);
            topFocusedPositionY = -topDefaultPositionY;

            _bottomPositionY = bottomPositionY = bottomDefaultPositionY = ComesGame.height + GetTextSize("123").Y;
            bottomFocusedPositionY = ComesGame.height - GetTextSize("123").Y - padding;

            blinkTimer.Start();

            optionsTitle = new UI.Text(ref font);
            optionsTitle.color = new Color(1f, 1f, 1f, 0f);
            optionsTitle.text = "paused";
            optionsTitle.SetAttribute("opacity", "0.0");

            AddOption("resume", "resume");
            AddOption("reset", "reset progress");
        }

        private Vector2 GetTextSize(string text)
        {
            return font.MeasureString(text);
        }

        public void ShowMenu(bool show = true)
        {
            focused = show;
        }

        public void Update(GameTime gameTime)
        {
            this.gameTime = gameTime;

            if (focused)
            {
                topPositionY = 0f;
                bottomPositionY = bottomFocusedPositionY;
            }
            else
            {
                topPositionY = topDefaultPositionY;
                bottomPositionY = bottomDefaultPositionY;
            }

            _topPositionY = Smooth.Lerp(_topPositionY, topPositionY, gameTime, 0.1f);
            _bottomPositionY = Smooth.Lerp(_bottomPositionY, bottomPositionY, gameTime, 0.1f);

            if (focused)
            {
                if (pUpPressed != Input.up)
                {
                    if (Input.up)
                    {
                        switch (selectedItem)
                        {
                            case 0:
                                selectedOption--;
                                break;
                        }
                    }

                    pUpPressed = Input.up;
                }

                if (pDownPressed != Input.down)
                {
                    if (Input.down)
                    {
                        switch (selectedItem)
                        {
                            case 0:
                                selectedOption++;
                                break;
                        }
                    }

                    pDownPressed = Input.down;
                }

                if (pLeftPressed != Input.left)
                {
                    if (Input.left)
                    {
                        selectedItem--;
                        blinkTimer.Restart();
                    }

                    pLeftPressed = Input.left;
                }

                if (pRightPressed != Input.right)
                {
                    if (Input.right)
                    {
                        selectedItem++;
                        blinkTimer.Restart();
                    }

                    pRightPressed = Input.right;
                }

                if (pAccept != Input.accept)
                {
                    if (Input.accept)
                    {
                        switch (selectedItem)
                        {
                            case 0:
                                switch (selectedOption)
                                {
                                    case 0:
                                        ShowMenu(false);
                                        break;
                                    case 1:
                                        ComesGame.properties.RemoveProperty("intro");
                                        ComesGame.Restart();
                                        break;
                                }
                                break;
                        }
                    }

                    pAccept = Input.accept;
                }

                if (pCancel != Input.cancel)
                {
                    if (Input.cancel)
                    {
                        if (selectedItem == 0)
                            ShowMenu(false);
                    }

                    pAccept = Input.accept;
                }
            }

            if (selectedItem < 0)
                selectedItem = menuItems.Count - 1;

            if (selectedItem > menuItems.Count - 1)
                selectedItem = 0;

            if (selectedOption < 0)
                selectedOption = options.Count - 1;

            if (selectedOption > options.Count - 1)
                selectedOption = 0;

            for (int i = 0; i < menuItems.Count; i++)
            {
                if (i == selectedItem)
                    menuItems[i].selected = true;
                else
                    menuItems[i].selected = false;
            }
        }

        public void Draw(ref SpriteBatch spriteBatch)
        {
            if (focused && selectedItem == 0)
                optionsTitle.SetAttribute("opacity", "0.1");
            else
                optionsTitle.SetAttribute("opacity", "0");

            optionsTitle.position.X = ComesGame.width / 2f - optionsTitle.GetTextSize().X / 2f;
            optionsTitle.position.Y = ComesGame.height / 2f - optionsTitle.GetTextSize().Y - (options.Count * optionsTitle.GetTextSize().Y / 2f) - 48f;

            optionsTitle.color = new Color(1f, 1f, 1f, Smooth.Lerp(optionsTitle.color.A / 255f, (float)Convert.ToDecimal(optionsTitle.GetAttribute("opacity")), gameTime, 0.15f));

            if (optionsTitle.color.A > 5)
                optionsTitle.Draw(ref spriteBatch);

            for (int i = 0; i < options.Count; i++)
            {
                UI.Text option = options[i];

                option.position.X = ComesGame.width / 2f - option.GetTextSize().X / 2f;
                option.SetAttribute("y", (ComesGame.height / 2f - option.GetTextSize().Y / 2f + i * (option.GetTextSize().Y + 8f) - (options.Count * option.GetTextSize().Y / 2f)).ToString());

                float offset = 0;

                if (focused && selectedItem == 0)
                {
                    if (i == selectedOption)
                    {
                        option.SetAttribute("opacity", "0.9");
                        offset = -2f;
                    }
                    else
                        option.SetAttribute("opacity", "0.2");
                } else
                    option.SetAttribute("opacity", "0");

                option.position.Y = Smooth.Lerp(option.position.Y, Convert.ToInt32(option.GetAttribute("y")) + offset, gameTime, 0.15f);
                option.color = new Color(1f, 1f, 1f, Smooth.Lerp(option.color.A / 255f, (float)Convert.ToDecimal(option.GetAttribute("opacity")), gameTime, 0.15f));

                if (optionsTitle.color.A > 5)
                    option.Draw(ref spriteBatch);
            }

            if (_topPositionY > -topFocusedPositionY + 0.5f)
                spriteBatch.Draw(TextureManager.textures["white"], new Rectangle(0, (int)_topPositionY, ComesGame.width, (int)topFocusedPositionY), new Color(0.1f, 0.1f, 0.1f, 1f));

            for (int i = 0; i < menuItems.Count; i++)
            {
                GameMenuItem menuItem = menuItems[i];

                int x = 0;
                int y = (int)_topPositionY;

                if (i > 0)
                {
                    int size = 0;

                    for (int j = 0; j < i; j++)
                        size += (int)GetTextSize(menuItems[j].text).X + padding;

                    x = size;
                }

                int width = (int)GetTextSize(menuItem.text).X + padding;
                int height = (int)GetTextSize(menuItem.text).Y + padding;

                if (menuItem.selected && focused)
                {
                    menuItem.color = 0.3f;
                    menuItem.color += (float)System.Math.Sin(blinkTimer.ElapsedMilliseconds / 100f) * 0.15f;
                }
                else
                    menuItem.color = 0.1f;

                menuItem._color = Smooth.Lerp(menuItem._color, menuItem.color, gameTime, 0.1f);

                if (_topPositionY > -topFocusedPositionY + 0.5f)
                {
                    spriteBatch.Draw(TextureManager.textures["white"], new Rectangle(x, y, width, height), new Color(menuItem._color, menuItem._color, menuItem._color, 1f));
                    spriteBatch.DrawString(font, menuItem.text, new Vector2(x + (int)(width / 2 - GetTextSize(menuItem.text).X / 2),
                                                                            y + (int)(height / 2 - GetTextSize(menuItem.text).Y / 2) - 1), Color.White);
                }
            }

            string fullString = "";

            fullString += ComesGame.properties.GetStringProperty("name");
            fullString += "     ";
            fullString += "LV";
            fullString += ComesGame.properties.GetIntProperty("level");
            fullString += "     ";
            fullString += "HP";
            fullString += ComesGame.properties.GetIntProperty("hp");
            fullString += "/";
            fullString += "20";

            if (_topPositionY > -topFocusedPositionY + 0.5f)
            {
                spriteBatch.Draw(TextureManager.textures["white"], new Rectangle(0, (int)_bottomPositionY, ComesGame.width, (int)GetTextSize("123").Y + padding), new Color(0.1f, 0.1f, 0.1f, 1f));
                spriteBatch.DrawString(font, fullString, new Vector2(padding, (int)_bottomPositionY + padding / 2 - 1), Color.White);
            }
        }
    }
}