﻿using System;

using Microsoft.Xna.Framework;

namespace Comes
{
    class Easing
    {
        public static float EaseInOutQuart(float t, float b, float c, float d)
        {
            t /= d / 2;
            if (t < 1) return c / 2 * t * t * t * t + b;
            t -= 2;
            return -c / 2 * (t * t * t * t - 2) + b;
        }
    }

    class Smooth
    {
        public static float Lerp(float value, float destination, GameTime gameTime, float smoothing = 0.05f)
        {
            return value += (smoothing * (float)gameTime.ElapsedGameTime.TotalSeconds * 100f) * (destination - value);
        }

        public static Vector2 Lerp(Vector2 vector, Vector2 destination, GameTime gameTime, float smoothing = 0.05f)
        {
            return new Vector2(Lerp(vector.X, destination.X, gameTime, smoothing), Lerp(vector.Y, destination.Y, gameTime, smoothing));
        }
    }

    class Camera
    {
        public Matrix matrix { get; private set; }

        public Vector2 position = Vector2.Zero;
        private Vector2 _position = Vector2.Zero;

        public Vector2 scale = Vector2.One;
        private Vector2 _scale = Vector2.One;

        public Vector2 origin = Vector2.Zero;
        private Vector2 _origin = Vector2.Zero;

        private Vector2 shake = Vector2.Zero;

        private float shakeIntensity = 0f;
        private float shakeDecay = 0f;

        private Random random = new Random();

        public float rotation = 0;
        private float _rotation = 0;

        public bool smooth = true;
        public float smoothing = 0.05f;

        public bool clamp = false;

        public Camera()
        {
            matrix = Matrix.Identity;
        }

        public void Shake(float intensity = 10f, float decay = 15f)
        {
            shakeIntensity = intensity;
            shakeDecay = decay;
        }

        public void Update(GameTime gameTime)
        {
            shake.X = (float)Math.Sin(Program.stopwatch.ElapsedMilliseconds * 10f + (random.Next(0, 10) - 5)) * shakeIntensity;
            shake.Y = (float)Math.Sin(Program.stopwatch.ElapsedMilliseconds * 5f + (random.Next(0, 10) - 5)) * shakeIntensity;

            if (smooth)
            {
                _position = Smooth.Lerp(_position, position, gameTime, smoothing);
                _scale = Smooth.Lerp(_scale, scale, gameTime, smoothing);
                _origin = Smooth.Lerp(_origin, origin, gameTime, smoothing);
                _rotation = Smooth.Lerp(_rotation, rotation, gameTime, smoothing);
            } else
            {
                _position = position;
                _scale = scale;
                _origin = origin;
                _rotation = rotation;
            }

            if (clamp)
                matrix =
                    Matrix.CreateScale(_scale.X, _scale.Y, 1) *
                    Matrix.CreateTranslation((int)-_origin.X * _scale.X, (int)-_origin.Y * _scale.Y, 0) *
                    Matrix.CreateRotationZ(MathHelper.ToRadians(_rotation)) *
                    Matrix.CreateTranslation((int)_position.X * _scale.X + (int)_origin.X + shake.X, (int)_position.Y * _scale.Y + (int)_origin.Y + shake.Y, 0);
            else
                matrix =
                        Matrix.CreateScale(_scale.X, _scale.Y, 1) *
                        Matrix.CreateTranslation(-_origin.X * _scale.X, -_origin.Y * _scale.Y, 0) *
                        Matrix.CreateRotationZ(MathHelper.ToRadians(_rotation)) *
                        Matrix.CreateTranslation(_position.X * _scale.X + _origin.X + shake.X, _position.Y * _scale.Y + _origin.Y + shake.Y, 0);

            if (shakeIntensity > 0)
                shakeIntensity -= (float)gameTime.ElapsedGameTime.TotalSeconds * shakeDecay;
        }
    }
}
