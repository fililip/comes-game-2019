﻿using System;
using System.Collections.Generic;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Comes
{
    namespace UI
    {
        abstract class Element
        {
            public Vector2 position = new Vector2(0, 0);
            public Color color = new Color(0.1f, 0.1f, 0.1f, 1f);
            private Dictionary<string, string> attributes = new Dictionary<string, string>();

            public string GetAttribute(string key)
            {
                return attributes.ContainsKey(key) ? attributes[key] : null;
            }

            public void SetAttribute(string key, string value)
            {
                attributes[key] = value;
            }

            public void RemoveAttribute(string key)
            {
                attributes.Remove(key);
            }

            public virtual void Update() { }
            public abstract void Draw(ref SpriteBatch spriteBatch);
        }

        class Panel : Element
        {
            public Vector2 size = new Vector2(32, 32);

            public override void Draw(ref SpriteBatch spriteBatch)
            {
                spriteBatch.Draw(TextureManager.textures["white"], new Rectangle(position.ToPoint(), size.ToPoint()), color);
            }
        }

        class Text : Element
        {
            private SpriteFont font;
            public string text = "";

            public Text(ref SpriteFont font)
            {
                this.font = font;
                color = Color.White;
            }

            public Vector2 GetTextSize()
            {
                return font.MeasureString(text);
            }

            public override void Draw(ref SpriteBatch spriteBatch)
            {
                spriteBatch.DrawString(font, text, position, color);
            }
        }

        class TextPanel : Element
        {
            public string text = "";
            private Panel panel = new Panel();
            private Text _text;
            private int padding = 16;

            public TextPanel(ref SpriteFont font)
            {
                _text = new Text(ref font);
            }

            public Vector2 GetSize()
            {
                return panel.size;
            }

            public override void Update()
            {
                _text.text = text;

                panel.position = position;
                panel.size = new Vector2(_text.GetTextSize().X + padding * 2, _text.GetTextSize().Y + padding * 2);

                _text.position = position + new Vector2(padding);
            }

            public override void Draw(ref SpriteBatch spriteBatch)
            {
                panel.Draw(ref spriteBatch);
                _text.Draw(ref spriteBatch);
            }
        }

        class TextBox : Element
        {
            public Vector2 size = new Vector2(32, 32);
            public string text { get; private set; } = "";
            public string placeholder = "";
            private Panel panel = new Panel();
            private Text _text;
            private Text _placeholder;
            private int padding = 16;

            private bool pLeftPressed = false;

            private bool focused = false;

            public TextBox(ref SpriteFont font)
            {
                _text = new Text(ref font);
                _placeholder = new Text(ref font);

                _placeholder.color = new Color(0.5f, 0.5f, 0.5f, 1f);
            }

            public Vector2 GetSize()
            {
                return panel.size;
            }

            private bool CheckMouseOver()
            {
                return Input.mousePosition.X > panel.position.X &&
                       Input.mousePosition.X < panel.position.X + panel.size.X &&
                       Input.mousePosition.Y > panel.position.Y &&
                       Input.mousePosition.Y < panel.position.Y + panel.size.Y;
            }

            public override void Update()
            {
                if (pLeftPressed != Input.LMB)
                {
                    if (Input.LMB)
                        focused = CheckMouseOver();

                    pLeftPressed = Input.LMB;
                }

                if (focused)
                {
                    _placeholder.color = new Color(0.5f, 0.5f, 0.5f, 0.9f);
                    _text.color = new Color(0.9f, 0.9f, 0.9f, 0.9f);
                    Input.disableKeyboard = true;

                    text = Input.textInput;
                } else
                {
                    _placeholder.color = new Color(0.5f, 0.5f, 0.5f, 0.5f);
                    _text.color = new Color(0.9f, 0.9f, 0.9f, 0.5f);
                    Input.disableKeyboard = false;
                }

                _text.text = text;
                _placeholder.text = placeholder;

                panel.position = position;
                panel.size = new Vector2(size.X + padding * 2, size.Y + padding * 2);

                _text.position = position + new Vector2(padding);
                _placeholder.position = _text.position;

                panel.color = color;
            }

            public override void Draw(ref SpriteBatch spriteBatch)
            {
                panel.Draw(ref spriteBatch);

                if (text == "")
                    _placeholder.Draw(ref spriteBatch);

                _text.Draw(ref spriteBatch);
            }
        }

        class Button : Element
        {
            public string text = "";
            private Panel panel = new Panel();
            private Text _text;
            public int padding = 16;

            private bool pLeftPressed = false;
            private int leftPressState = 0;

            private bool pMiddlePressed = false;
            private int middlePressState = 0;

            private bool pRightPressed = false;
            private int rightPressState = 0;

            public Action leftPressCallback = () => { };
            public Action middlePressCallback = () => { };
            public Action rightPressCallback = () => { };

            public Button(ref SpriteFont font)
            {
                _text = new Text(ref font);
            }

            public void SetTextColor(Color color)
            {
                _text.color = color;
            }

            public Color GetTextColor()
            {
                return _text.color;
            }

            public Vector2 GetSize()
            {
                return panel.size;
            }

            private bool CheckMouseOver()
            {
                return Input.mousePosition.X > panel.position.X &&
                       Input.mousePosition.X < panel.position.X + panel.size.X &&
                       Input.mousePosition.Y > panel.position.Y &&
                       Input.mousePosition.Y < panel.position.Y + panel.size.Y;
            }

            public override void Update()
            {
                _text.text = text;

                panel.position = position;
                panel.size = new Vector2(_text.GetTextSize().X + padding * 2, _text.GetTextSize().Y + padding * 2);

                _text.position = position + new Vector2(padding);

                if (leftPressCallback != null)
                {
                    if (pLeftPressed != Input.LMB)
                    {
                        if (CheckMouseOver())
                        {
                            if (!pLeftPressed && Input.LMB && leftPressState == 0)
                                leftPressState = 1;

                            if (pLeftPressed && !Input.LMB && leftPressState == 1)
                                leftPressState = 2;
                        }

                        pLeftPressed = Input.LMB;
                    }
                }

                if (middlePressCallback != null)
                {
                    if (pMiddlePressed != Input.MMB)
                    {
                        if (CheckMouseOver())
                        {
                            if (!pMiddlePressed && Input.MMB && middlePressState == 0)
                                middlePressState = 1;

                            if (!pMiddlePressed && !Input.MMB && middlePressState == 1)
                                middlePressState = 2;
                        }

                        pMiddlePressed = Input.MMB;
                    }
                }

                if (rightPressCallback != null)
                {
                    if (pRightPressed != Input.RMB)
                    {
                        if (CheckMouseOver())
                        {
                            if (!pRightPressed && Input.RMB && rightPressState == 0)
                                rightPressState = 1;

                            if (pRightPressed && !Input.RMB && rightPressState == 1)
                                rightPressState = 2;
                        }

                        pRightPressed = Input.RMB;
                    }
                }

                if (leftPressState >= 2)
                {
                    leftPressCallback();
                    leftPressState = 0;
                }

                if (middlePressState >= 2)
                {
                    middlePressCallback();
                    middlePressState = 0;
                }

                if (rightPressState >= 2)
                {
                    rightPressCallback();
                    rightPressState = 0;
                }

                panel.color = color;

                if (CheckMouseOver())
                {
                    panel.color.R += 25;
                    panel.color.G += 25;
                    panel.color.B += 25;

                    if (leftPressState > 0)
                    {
                        panel.color.R += 35;
                        panel.color.G += 35;
                        panel.color.B += 35;
                    }
                } else
                    leftPressState = middlePressState = rightPressState = 0;
            }

            public override void Draw(ref SpriteBatch spriteBatch)
            {
                panel.Draw(ref spriteBatch);
                _text.Draw(ref spriteBatch);
            }
        }

        class ScrollBox : Element
        {
            private Panel panel = new Panel();
            private Panel scrollBarPanel = new Panel();
            private Panel scrollBarButton = new Panel();

            private RasterizerState rasterizerState = new RasterizerState() { ScissorTestEnable = true };

            private List<Element> elements = new List<Element>();

            public override void Update()
            {
                foreach (Element element in elements)
                    element.Update();
            }

            public override void Draw(ref SpriteBatch spriteBatch)
            {
                panel.Draw(ref spriteBatch);

                spriteBatch.End();

                Rectangle rectangle = new Rectangle(0, 0, 0, 0);

                spriteBatch.Begin(blendState: BlendState.NonPremultiplied, rasterizerState: rasterizerState, samplerState: SamplerState.PointClamp);
                spriteBatch.GraphicsDevice.ScissorRectangle = rectangle;

                foreach (Element element in elements)
                    element.Draw(ref spriteBatch);

                spriteBatch.End();

                spriteBatch.Begin(blendState: BlendState.NonPremultiplied, samplerState: SamplerState.PointClamp);

                scrollBarPanel.Draw(ref spriteBatch);
                scrollBarButton.Draw(ref spriteBatch);
            }
        }

        class Canvas
        {
            private List<Element> elements = new List<Element>();

            public void AddElement(Element element)
            {
                elements.Add(element);
            }

            public Element GetElement(int index)
            {
                return elements[index];
            }

            public Element[] GetElements()
            {
                return elements.ToArray();
            }

            public void Update()
            {
                foreach (Element element in elements)
                    element.Update();
            }

            public void Draw(ref SpriteBatch spriteBatch)
            {
                foreach (Element element in elements)
                    element.Draw(ref spriteBatch);
            }
        }
    }
}