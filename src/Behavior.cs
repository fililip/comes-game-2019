﻿using System;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.Xna.Framework;

namespace Comes
{
    [Serializable]
    abstract class Behavior
    {
        public bool collision = false;
        public bool active { get; protected set; } = true;
        protected bool pressed = false;

        public abstract void OnInteraction();

        public virtual void Loop(GameTime gameTime, Scene scene, MapObject self, bool acceptKeyEvent = true)
        {
            if (!pressed && Input.accept)
            {
                pressed = true;

                if (collision && acceptKeyEvent)
                    OnInteraction();

                new Task(() =>
                {
                    while (Input.accept)
                        Thread.Sleep(1);

                    pressed = false;
                }).Start();
            }
        }
    }

    [Serializable]
    class DummyBehavior : Behavior
    {
        private Scenes.Game scene;
        private MapObject self;

        Random r = new Random();

        public override void OnInteraction()
        {
            scene.projectileSystem.ammo = scene.projectileSystem.maxAmmo;
            scene.captionSystem.AddCaption("Ammunition restored", 1250);
            active = false;
        }

        public override void Loop(GameTime gameTime, Scene scene, MapObject self, bool acceptKeyEvent = true)
        {
            base.Loop(gameTime, scene, self, acceptKeyEvent);

            this.scene = (Scenes.Game)scene;
            this.self = self;
        }
    }
}