﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Comes
{
    namespace Scenes
    {
        class Shilling : Scene
        {
            private SpriteFont font;

            private Stopwatch stopwatch = new Stopwatch();
            private List<string> strings = new List<string>
            {
                "NEI DB COMKA MAKES\nNEI DB GAMES TM\nPRESENTS:",
                "A GAME BY",
                "DJ COMEX"
            };
            private bool fadeIn = true;
            private int currentString = 0;
            private float opacity = 0f;

            public Shilling(ref GraphicsDeviceManager graphics, ref SpriteBatch spriteBatch, ref ContentManager contentManager)
            {
                this.graphics = graphics;
                this.spriteBatch = spriteBatch;
                this.contentManager = contentManager;

                sceneID = 0;

                font = contentManager.Load<SpriteFont>("fonts/font");
            }

            protected override void Start()
            {
                if (ComesGame.properties.HasProperty("intro"))
                {
                    base.BeforeQuit();
                    sceneID = 1;
                } else
                {
                    //SoundManager.LoadSound("intro.wav");
                    //SoundManager.PlaySound("intro.wav");
                }

                stopwatch.Reset();
            }

            public override int Update(GameTime gameTime)
            {
                base.BeforeUpdate();

                if (GamePad.GetState(PlayerIndex.One).Buttons.A == ButtonState.Pressed ||
                    Keyboard.GetState().IsKeyDown(Keys.Enter) ||
                    currentString > strings.Count - 1)
                {
                    base.BeforeQuit();
                    return 1;
                }

                if (fadeIn && stopwatch.ElapsedMilliseconds == 0)
                {
                    if (opacity < 1)
                        opacity += (float)gameTime.ElapsedGameTime.TotalSeconds;
                    else
                    {
                        opacity = 1f;

                        stopwatch.Start();
                    }
                }

                if (!fadeIn && stopwatch.ElapsedMilliseconds == 0)
                {
                    if (opacity > 0)
                        opacity -= (float)gameTime.ElapsedGameTime.TotalSeconds;
                    else
                    {
                        opacity = 0f;

                        fadeIn = !fadeIn;

                        if (fadeIn)
                        {
                            currentString++;

                            if (currentString == 2)
                            {
                                /*SoundManager.LoadSound("comex.wav", true);
                                SoundManager.PlaySound("comex.wav");

                                SoundManager.AddDSP("comex.wav", 0, FMOD.DSP_TYPE.SFXREVERB);
                                SoundManager.SetDSPParameter("comex.wav", 0, 0, 3300);

                                SoundManager.AddDSP("comex.wav", 1, FMOD.DSP_TYPE.ECHO);
                                SoundManager.SetDSPParameter("comex.wav", 1, 0, 100);
                                SoundManager.SetDSPParameter("comex.wav", 1, 1, 10);

                                new Task(() =>
                                {
                                    Thread.Sleep(1400);
                                    SoundManager.PauseSound("comex.wav");
                                }).Start();*/
                            }
                        }
                    }
                }

                if (stopwatch.ElapsedMilliseconds >= 3500)
                {
                    fadeIn = !fadeIn;

                    if (fadeIn)
                        currentString++;

                    stopwatch.Reset();
                }

                return sceneID;
            }

            public override void Draw(ref SpriteBatch spriteBatch)
            {
                spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.NonPremultiplied);

                if (currentString <= strings.Count - 1)
                    spriteBatch.DrawString(font, strings[currentString],
                                           new Vector2(ComesGame.width / 2 - (int)font.MeasureString(strings[currentString]).X / 2,
                                                       ComesGame.height / 2 - (int)font.MeasureString(strings[currentString]).Y / 2),
                                           new Color(1f, 1f, 1f, opacity));

                spriteBatch.End();
            }
        }
        
        class Intro : Scene
        {
            private List<string> strings = new List<string>
            {
                "Kiedys comes byl zywy.",
                "comes huj...",
                "Comes mieszkal w Lub Linie.",
                "comes mial Poduszeczki...",
                "Comes chcial ruchac...",
                "Ruchac poduszeczki.",
                "Az kiedys....",
                "Ktos mu zajebal..."
            };

            private List<string> shakeStrings = new List<string>
            {
                "obudz sie, comes....",
                "musimy odnalezc...",
                "zloczynce..."
            };

            private SpriteFont font;
            private TextBlitter textBlitter;

            private List<Texture2D> scenes = new List<Texture2D>();
            private int currentBackground = 0;

            private Stopwatch fadeTimer = new Stopwatch();
            private bool fadeIn = true;
            private Direction direction = Direction.Up;
            private float opacity = 0f;
            private float offsetX = 0f;
            private float offsetY = 0f;

            public Intro(ref GraphicsDeviceManager graphics, ref SpriteBatch spriteBatch, ref ContentManager contentManager)
            {
                this.graphics = graphics;
                this.spriteBatch = spriteBatch;
                this.contentManager = contentManager;

                sceneID = 1;

                scenes.Add(TextureManager.textures["sceneries/1a"]);
                scenes.Add(TextureManager.textures["sceneries/1b"]);
                scenes.Add(TextureManager.textures["sceneries/1c"]);
                scenes.Add(TextureManager.textures["sceneries/2"]);
                scenes.Add(TextureManager.textures["sceneries/3"]);
                scenes.Add(TextureManager.textures["sceneries/4"]);

                font = contentManager.Load<SpriteFont>("fonts/font");
                textBlitter = new TextBlitter(ref font);
            }

            protected override void Start()
            {
                if (ComesGame.properties.HasProperty("intro"))
                {
                    base.BeforeQuit();
                    sceneID = 2;
                }

                foreach (string s in strings)
                    textBlitter.AddString(s, 950);

                for (int i = 0; i < shakeStrings.Count; i++)
                {
                    string shakeString = shakeStrings[i];

                    Text text = new Text();

                    foreach (char c in shakeString)
                    {
                        TextCharacter character = new TextCharacter();

                        character.character = c;
                        character.shake = true;

                        text.characters.Add(character);
                    }

                    textBlitter.AddText(text);
                }

                textBlitter.SetActive();
                fadeTimer.Start();
            }

            public override int Update(GameTime gameTime)
            {
                base.BeforeUpdate();

                if (GamePad.GetState(PlayerIndex.One).Buttons.A == ButtonState.Pressed ||
                    Keyboard.GetState().IsKeyDown(Keys.Enter))
                {
                    base.BeforeQuit();
                    return 2;
                }

                textBlitter.Update(gameTime);
                textBlitter.position = new Vector2(ComesGame.width / 2 - textBlitter.GetStringSize().X / 2, ComesGame.height / 2 - textBlitter.GetStringSize().Y / 2);

                if (textBlitter.finished)
                {
                    base.BeforeQuit();
                    return 2;
                }

                if (fadeIn)
                {
                    if (opacity < 0.25f)
                        opacity += (float)gameTime.ElapsedGameTime.TotalSeconds / 1.5f;
                    else
                        opacity = 0.25f;
                } else
                {
                    if (opacity > 0f)
                        opacity -= (float)gameTime.ElapsedGameTime.TotalSeconds * 4f;
                    else
                        opacity = 0f;
                }

                switch (direction)
                {
                    case Direction.Up:
                        offsetY -= (float)gameTime.ElapsedGameTime.TotalSeconds * 5f;
                        break;
                    case Direction.Down:
                        offsetY += (float)gameTime.ElapsedGameTime.TotalSeconds * 5f;
                        break;
                    case Direction.Left:
                        offsetX -= (float)gameTime.ElapsedGameTime.TotalSeconds * 5f;
                        break;
                    case Direction.Right:
                        offsetX += (float)gameTime.ElapsedGameTime.TotalSeconds * 5f;
                        break;
                }

                if (fadeTimer.ElapsedMilliseconds >= 3500)
                {
                    fadeIn = !fadeIn;

                    if (fadeIn)
                        fadeIn = false;

                    fadeTimer.Restart();
                }

                if (fadeTimer.ElapsedMilliseconds >= 200)
                {
                    if (!fadeIn)
                    {
                        fadeIn = true;
                        currentBackground++;

                        offsetX = new Random().Next(0, 40) - 20;
                        offsetY = new Random().Next(0, 40) - 20;

                        direction = (Direction)new Random().Next(1, 4);

                        fadeTimer.Restart();
                    }
                }

                if (currentBackground < 0)
                    currentBackground = scenes.Count - 1;

                if (currentBackground >= scenes.Count - 1)
                    currentBackground = 0;

                return sceneID;
            }

            public override void Draw(ref SpriteBatch spriteBatch)
            {
                spriteBatch.Begin(blendState: BlendState.NonPremultiplied);

                spriteBatch.Draw(scenes[currentBackground],
                    new Vector2(ComesGame.width / 2f - scenes[currentBackground].Width / 2f + offsetX, ComesGame.height / 2f - scenes[currentBackground].Height / 2f + offsetY),
                    new Color(1, 1, 1, opacity));

                spriteBatch.End();

                spriteBatch.Begin(blendState: BlendState.NonPremultiplied);

                textBlitter.Draw(ref spriteBatch);

                spriteBatch.End();
            }
        }

        class Game : Scene
        {
            private Player player;

            public CaptionSystem captionSystem;
            public GameMenu gameMenu;
            public OnScreenDisplay onScreenDisplay;

            public ProjectileSystem projectileSystem;

            private SpriteFont font;

            public int fadeIn { get; private set; } = 1;
            private float _fadeOpacity = 1f;
            private float fadeOpacity = 0f;
            private Color fadeColor = Color.Black;

            private bool pMenuPressed = false;

            private bool attack = false;
            private Stopwatch attackStopwatch = new Stopwatch();
            private bool pAttackPressed = false;

            private string[] args;
            public Map map = new Map();

            private Camera camera = new Camera();

            private Effect chromaEffect;
            private RenderTarget2D chromaRenderTarget;

            private Effect blurEffect;
            private RenderTarget2D hBlurRenderTarget;
            private RenderTarget2D vBlurRenderTarget;

            private ParticleEmitter particleSystem = new ParticleEmitter();

            public Game(ref GraphicsDeviceManager graphics, ref SpriteBatch spriteBatch, ref ContentManager contentManager, string[] args)
            {
                this.args = args;
                this.graphics = graphics;
                this.spriteBatch = spriteBatch;
                this.contentManager = contentManager;

                sceneID = 2;

                player = new Player();

                camera.clamp = true;
                camera.origin = new Vector2(ComesGame.width / 2f, ComesGame.height / 2f);

                font = contentManager.Load<SpriteFont>("fonts/font_small");

                captionSystem = new CaptionSystem(ref font);
                gameMenu = new GameMenu(ref font);
                onScreenDisplay = new OnScreenDisplay(ref font);

                projectileSystem = new ProjectileSystem();

                particleSystem.size = new Vector2(512, 512);

                map.Load("testmap");

                map.name = "map_intro";

                /*MapObject topWall = new MapObject(new Rect(0, -64, ComesGame.width, 64));
                MapObject bottomWall = new MapObject(new Rect(0, ComesGame.height, ComesGame.width, 64));

                MapObject leftWall = new MapObject(new Rect(-64, 0, 64, ComesGame.height));
                MapObject rightWall = new MapObject(new Rect(ComesGame.width, 0, 64, ComesGame.height));

                map.map.Add(topWall);
                map.map.Add(bottomWall);
                map.map.Add(leftWall);
                map.map.Add(rightWall);*/

                DiscordRichPresence.SetPresence(new Presence()
                {
                    details = "...",
                    state = "..."
                });

                chromaEffect = contentManager.Load<Effect>("shaders/chroma");
                chromaRenderTarget = new RenderTarget2D(graphics.GraphicsDevice, ComesGame.width, ComesGame.height);

                blurEffect = contentManager.Load<Effect>("shaders/blur");
                hBlurRenderTarget = new RenderTarget2D(graphics.GraphicsDevice, ComesGame.width, ComesGame.height);
                vBlurRenderTarget = new RenderTarget2D(graphics.GraphicsDevice, ComesGame.width, ComesGame.height);

                chromaEffect.Parameters["resolution"].SetValue(new Vector2(ComesGame.width, ComesGame.height));
                blurEffect.Parameters["resolution"].SetValue(new Vector2(ComesGame.width, ComesGame.height));

                chromaEffect.Parameters["m"].SetValue(1.5f);
            }

            protected override void Start()
            {
                /*if (ComesGame.properties.HasProperty("ammo_basic"))
                    projectileSystem.ammo = ComesGame.properties.GetIntProperty("ammo_basic");

                if (ComesGame.properties.HasProperty("player_position_x") && ComesGame.properties.HasProperty("player_position_y"))
                    player.SetPosition(new Vector2(ComesGame.properties.GetIntProperty("player_position_x"), ComesGame.properties.GetIntProperty("player_position_y")));*/

                switch (map.name) {
                    case "map_intro":
                        if (ComesGame.properties.HasProperty("intro"))
                            ComesGame.windowTitle = "Thief";

                        if (!ComesGame.properties.HasProperty("intro"))
                        {
                            new Task(() =>
                            {
                                Thread.Sleep(1000);
                                fadeColor = Color.Black;

                                ComesGame.properties.SetIntProperty("intro", 1);
                                Thread.Sleep(500);
                                onScreenDisplay.ShowText("Chapter 1: Thief");
                                ComesGame.windowTitle = "Thief";
                            }).Start();
                        }
                        break;
                }

                Sound sound = SoundManager.LoadSound("noise.ogg", true);
                //SoundManager.PlaySound(sound);

                SoundManager.AddDSP(sound, 0, FMOD.DSP_TYPE.LOWPASS);
                SoundManager.AddDSP(sound, 1, FMOD.DSP_TYPE.SFXREVERB);

                SoundManager.SetDSPParameter(sound, 0, 0, 1025);
                SoundManager.SetDSPParameter(sound, 1, 0, 4700);
                SoundManager.SetDSPParameter(sound, 1, 12, -500);

                camera.scale = new Vector2(6, 6);

                //SoundManager.PauseSound(sound);

                new Task(() =>
                {
                    Thread.Sleep(100);
                    camera.scale = new Vector2(1, 1);
                }).Start();

                camera.smooth = false;
            }

            public override int Update(GameTime gameTime)
            {
                base.BeforeUpdate();

                if (!gameMenu.focused)
                {
                    if (fadeOpacity < 0.5f)
                    {
                        player.ProcessInput(gameTime);

                        if (pAttackPressed != Input.attack)
                        {
                            attackStopwatch.Reset();
                            attack = Input.attack;

                            if (attack)
                                attackStopwatch.Start();

                            pAttackPressed = Input.attack;
                        }
                    }

                    fadeIn = 1;
                }
                else
                {
                    fadeIn = 2;
                    player.StopInput();
                }

                if (chromaRenderTarget.Width != ComesGame.width || chromaRenderTarget.Height != ComesGame.height)
                {
                    chromaRenderTarget = new RenderTarget2D(graphics.GraphicsDevice, ComesGame.width, ComesGame.height);

                    hBlurRenderTarget = new RenderTarget2D(graphics.GraphicsDevice, ComesGame.width, ComesGame.height);
                    vBlurRenderTarget = new RenderTarget2D(graphics.GraphicsDevice, ComesGame.width, ComesGame.height);

                    chromaEffect.Parameters["resolution"].SetValue(new Vector2(ComesGame.width, ComesGame.height));
                    blurEffect.Parameters["resolution"].SetValue(new Vector2(ComesGame.width, ComesGame.height));
                }

                map.Update(gameTime, this, ref player, !gameMenu.focused);

                if (Input.mousePosition.X > 0 && Input.mousePosition.X < ComesGame.width &&
                    Input.mousePosition.Y > 0 && Input.mousePosition.Y < ComesGame.height)
                    camera.position =
                        new Vector2(((Input.mousePosition.X - (ComesGame.width / 2f)) / (ComesGame.width / 2f)) * 40f,
                                    ((Input.mousePosition.Y - (ComesGame.height / 2f)) / (ComesGame.height / 2f)) * 40f);

                if (Input.mousePosition.X > 0 && Input.mousePosition.Y > 0 &&
                    Input.mousePosition.X < ComesGame.width && Input.mousePosition.Y < ComesGame.height && !gameMenu.focused)
                    camera.position += new Vector2(-player.GetPosition().X + ComesGame.width / 2, -player.GetPosition().Y + ComesGame.height / 2 - 32);
                else
                    camera.position = new Vector2(-player.GetPosition().X + ComesGame.width / 2, -player.GetPosition().Y + ComesGame.height / 2 - 32);

                player.Update(gameTime);

                particleSystem.position = player.GetPosition() + new Vector2(16, 32);
                particleSystem.Update(gameTime);

                //chromaEffect.Parameters["mat"].SetValue(camera.matrix * Matrix.CreateOrthographicOffCenter(0, ComesGame.width, ComesGame.height, 0, 0, -1));

                ComesGame.properties.SetIntProperty("player_position_x", (int)player.GetPosition().X);
                ComesGame.properties.SetIntProperty("player_position_y", (int)player.GetPosition().Y);

                camera.Update(gameTime);

                if (!camera.smooth)
                    camera.smooth = true;

                if (fadeOpacity <= 0.9f)
                {
                    if (pMenuPressed != Input.menu)
                    {
                        if (Input.menu)
                            gameMenu.ShowMenu(!gameMenu.focused);

                        pMenuPressed = Input.menu;
                    }
                }

                switch (fadeIn)
                {
                    case 0:
                        fadeOpacity = 1;
                        break;
                    case 1:
                        fadeOpacity = 0;
                        break;
                    case 2:
                        fadeOpacity = 0.9f;
                        break;
                }

                _fadeOpacity = Smooth.Lerp(_fadeOpacity, fadeOpacity, gameTime, 0.05f);
                
                camera.scale.X += 0.25f * (float)Input.scrollValue;
                camera.scale.X = Math.Clamp(camera.scale.X, 0.25f, 4f);
                camera.scale.Y = camera.scale.X;

                if (attack)
                {
                    if (attackStopwatch.ElapsedMilliseconds > 50)
                    {
                        projectileSystem.Shoot(player.GetDirection(), player.GetAnchorPosition());
                        ComesGame.properties.SetIntProperty("ammo_basic", projectileSystem.ammo);
                        attackStopwatch.Restart();
                    }
                }

                gameMenu.Update(gameTime);
                captionSystem.Update(gameTime);
                onScreenDisplay.Update(gameTime);

                projectileSystem.Update(gameTime);

                Input.FinishScroll();

                return sceneID;
            }

            public override void Draw(ref SpriteBatch spriteBatch)
            {
                //graphics.GraphicsDevice.SetRenderTarget(chromaRenderTarget);

                spriteBatch.Begin(transformMatrix: camera.matrix, blendState: BlendState.NonPremultiplied, samplerState: SamplerState.PointClamp);

                player.Draw(ref spriteBatch);

                map.Draw(ref spriteBatch);

                projectileSystem.Draw(ref spriteBatch);

                particleSystem.Draw(ref spriteBatch);

                spriteBatch.End();

                /* -- !!! ATENCIONE !!! -- */

                /* graphics.GraphicsDevice.SetRenderTarget(hBlurRenderTarget);

                spriteBatch.Begin(blendState: BlendState.Additive, sortMode: SpriteSortMode.Immediate, effect: chromaEffect);

                spriteBatch.Draw(chromaRenderTarget, new Rectangle(0, 0, ComesGame.width, ComesGame.height), Color.White);

                spriteBatch.End(); */

                /* -- a juz nic :) -- */

                /* -- !!! ATENCIONE 1 !!! -- */

                /* graphics.GraphicsDevice.SetRenderTarget(vBlurRenderTarget);

                blurEffect.Parameters["horizontal"].SetValue(1);

                spriteBatch.Begin(blendState: BlendState.Additive, sortMode: SpriteSortMode.Immediate, effect: blurEffect);

                spriteBatch.Draw(hBlurRenderTarget, new Rectangle(0, 0, ComesGame.width, ComesGame.height), Color.White);

                spriteBatch.End(); */

                /* -- a juz nic :) -- */

                /* -- !!! ATENCIONE 2 !!! -- */

                /* graphics.GraphicsDevice.SetRenderTarget(null);

                blurEffect.Parameters["horizontal"].SetValue(0);

                spriteBatch.Begin(blendState: BlendState.Additive, sortMode: SpriteSortMode.Immediate, effect: blurEffect);

                spriteBatch.Draw(vBlurRenderTarget, new Rectangle(0, 0, ComesGame.width, ComesGame.height), Color.White);

                spriteBatch.End(); */

                /* -- a juz nic :) -- */

                captionSystem.Draw(ref spriteBatch);

                spriteBatch.Begin(blendState: BlendState.NonPremultiplied, samplerState: SamplerState.PointClamp);

                string text = projectileSystem.ammo.ToString() + " / " + projectileSystem.maxAmmo.ToString();

                Color color = new Color(1f, 1f, 1f, 0.7f);

                if (projectileSystem.ammo < 20)
                    color = new Color(1f, 0.2f, 0.2f, 0.7f);

                int padding = 16;
                int width = (int)font.MeasureString(text).X + padding;
                int height = (int)font.MeasureString(text).Y + padding;

                spriteBatch.Draw(TextureManager.textures["white"],
                                 new Rectangle(8, ComesGame.height - height - 8, width, height), new Color(0.1f, 0.1f, 0.1f, 0.7f));

                spriteBatch.DrawString(font, text, new Vector2(8 + padding / 2 + 1, ComesGame.height - padding / 2 - height / 2 - 8 - 1), color);

                onScreenDisplay.Draw(ref spriteBatch);

                spriteBatch.Draw(TextureManager.textures["white"], new Rectangle(0, 0, ComesGame.width, ComesGame.height), new Color(fadeColor.R, fadeColor.G, fadeColor.B, _fadeOpacity));

                gameMenu.Draw(ref spriteBatch);

                spriteBatch.End();
            }
        }
    }
}