﻿using System;
using System.Collections.Generic;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Comes
{
    public enum Direction { None, Up, Down, Left, Right, UpLeft, UpRight, DownLeft, DownRight };

    class Projectile
    {
        public Vector2 startPosition;
        public Vector2 position;
        public Direction direction;
        public float speed;
        public bool alive = true;
    }

    class ProjectileSystem
    {
        private List<Projectile> projectiles = new List<Projectile>();

        public int ammo = 0;
        public int maxAmmo = 50;

        public ProjectileSystem()
        {
            ammo = maxAmmo;
        }

        public void Shoot(Direction direction, Vector2 position, float speed = 5f)
        {
            if (direction != Direction.None && ammo > 0)
            {
                Projectile projectile = new Projectile();

                projectile.startPosition = position;
                projectile.position = position;
                projectile.direction = direction;
                projectile.speed = speed * 100f;

                projectiles.Add(projectile);
                ammo--;
            }
        }

        public void Update(GameTime gameTime)
        {
            foreach (Projectile projectile in projectiles)
            {
                float delta = projectile.speed * (float)gameTime.ElapsedGameTime.TotalSeconds;

                switch (projectile.direction)
                {
                    case Direction.Up:
                        projectile.position.Y += -delta;
                        break;
                    case Direction.Down:
                        projectile.position.Y += delta;
                        break;
                    case Direction.Left:
                        projectile.position.X += -delta;
                        break;
                    case Direction.Right:
                        projectile.position.X += delta;
                        break;
                    case Direction.UpLeft:
                        projectile.position.Y += -delta;
                        projectile.position.X += -delta;
                        break;
                    case Direction.UpRight:
                        projectile.position.Y += -delta;
                        projectile.position.X += delta;
                        break;
                    case Direction.DownLeft:
                        projectile.position.Y += delta;
                        projectile.position.X += -delta;
                        break;
                    case Direction.DownRight:
                        projectile.position.Y += delta;
                        projectile.position.X += delta;
                        break;
                }

                if (Math.Abs(projectile.position.X - projectile.startPosition.X) > 400 || Math.Abs(projectile.position.Y - projectile.startPosition.Y) > 400)
                {
                    projectile.alive = false;
                    continue;
                }
            }

            for (int i = 0; i < projectiles.Count; i++)
            {
                if (!projectiles[i].alive)
                {
                    projectiles.RemoveAt(i);
                    i--;
                }
            }
        }

        public void Draw(ref SpriteBatch spriteBatch)
        {
            foreach (Projectile projectile in projectiles)
            {
                spriteBatch.Draw(TextureManager.textures["white"], new Rectangle((int)projectile.position.X - 4, (int)projectile.position.Y - 4, 8, 8), Color.White);
            }
        }
    }
}