﻿using System.Collections.Generic;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Comes
{
    class Caption
    {
        public Vector2 _position = Vector2.Zero;
        public Vector2 position = Vector2.Zero;

        public float _opacity = 0f;
        public float opacity = 1f;

        public string text = "";

        public bool alive = true;

        public long time = 0;
        public long timeout = 3000;
    }

    class CaptionSystem
    {
        private SpriteFont font;
        public List<Caption> captions = new List<Caption>();

        public Vector2 position;
        public float size;

        private float lineHeight = 0f;
        private float boxHeight = 0f;
        private float boxOpacity = 0f;

        private float lastBoxHeight = 38f;

        public bool showBackground = true;

        private int _boxWidth = 0;
        private float _boxHeight = 0f;

        private RasterizerState rasterizerState = new RasterizerState() { ScissorTestEnable = true };

        private GameTime gameTime = new GameTime();

        public CaptionSystem(ref SpriteFont font)
        {
            position = new Vector2(ComesGame.width / 2f, ComesGame.height);

            this.font = font;
            lineHeight = font.MeasureString(" ").Y;
        }

        private Vector2 GetTextSize(string text)
        {
            return font.MeasureString(text);
        }

        public void AddCaption(string text, long timeout = 3000)
        {
            Vector2 textSize = GetTextSize(text);

            Caption caption = new Caption();
            caption.text = text;

            caption._position = caption.position = new Vector2(position.X - textSize.X / 2f,
                                                               position.Y - textSize.Y + lineHeight * text.Split('\n').Length);

            caption._position.X = caption.position.X = (int)caption.position.X;

            caption.time = Program.stopwatch.ElapsedMilliseconds;
            caption.timeout = timeout;

            lastBoxHeight = 26f + lineHeight * text.Split('\n').Length;
            if (boxOpacity <= 0.05f)
                _boxHeight = boxHeight = lastBoxHeight;

            captions.Insert(0, caption);
        }

        public void Update(GameTime gameTime)
        {
            size = ComesGame.width - 42;

            this.gameTime = gameTime;
            _boxWidth = (int)size;

            if (_boxWidth > 720)
                _boxWidth = 720;

            for (int i = 0; i < captions.Count; i++)
            {
                Caption caption = captions[i];

                if (i > 0)
                    caption.position.Y = captions[i - 1].position.Y - GetTextSize(caption.text).Y - 12f;
                else
                {
                    if (captions.Count == 1)
                        caption._position.Y = caption.position.Y = position.Y - GetTextSize(caption.text).Y - 32f;
                    else
                        caption.position.Y = position.Y - GetTextSize(caption.text).Y - 32f;
                }

                if (caption.alive && Program.stopwatch.ElapsedMilliseconds - caption.time > caption.timeout)
                    caption.alive = false;

                if (captions.Count > 5)
                {
                    for (int j = 5; j < captions.Count; j++)
                        captions[j].alive = false;
                }

                if (!caption.alive)
                {
                    caption.opacity = 0f;
                    if (caption._opacity < 0.05f)
                        captions.Remove(caption);
                }

                caption._position.Y = Smooth.Lerp(caption._position.Y, caption.position.Y, gameTime, 0.1f);
                caption._opacity = Smooth.Lerp(caption._opacity, caption.opacity, gameTime, 0.15f);
            }
        }

        public void Draw(ref SpriteBatch spriteBatch)
        {
            if (captions.Count == 0)
            {
                boxOpacity = Smooth.Lerp(boxOpacity, 0f, gameTime, 0.1f);
                boxHeight = Smooth.Lerp(boxHeight, lastBoxHeight, gameTime, 0.1f);
            }
            else
            {
                boxOpacity = Smooth.Lerp(boxOpacity, 0.75f, gameTime, 0.1f);

                _boxHeight = 0;

                for (int i = 0; i < captions.Count; i++)
                {
                    Caption caption = captions[i];

                    _boxHeight += GetTextSize(caption.text).Y + 12f;
                }

                _boxHeight += 12;

                boxHeight = Smooth.Lerp(boxHeight, _boxHeight, gameTime, 0.1f);
            }

            if (!showBackground)
                boxOpacity = 0f;

            Rectangle rectangle = new Rectangle((int)position.X - _boxWidth / 2, (int)position.Y - 32 - (int)boxHeight + 12, _boxWidth, (int)boxHeight);
            
            spriteBatch.Begin(blendState: BlendState.NonPremultiplied, rasterizerState: rasterizerState, samplerState: SamplerState.PointClamp);
            spriteBatch.GraphicsDevice.ScissorRectangle = rectangle;

            spriteBatch.Draw(TextureManager.textures["white"], rectangle,
                             new Color(0.05f, 0.05f, 0.05f, boxOpacity));

            foreach (Caption caption in captions)
                spriteBatch.DrawString(font, caption.text, caption._position,
                                       new Color(1f, 1f, 1f, caption._opacity));

            spriteBatch.End();
        }
    }
}
