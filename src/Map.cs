﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Comes
{
    [Serializable]
    class Rect
    {
        public int X = 0, Y = 0, Width = 0, Height = 0;

        public Rect(int x, int y, int width, int height)
        {
            X = x;
            Y = y;
            Width = width;
            Height = height;
        }
    }

    [Serializable]
    class MapObject
    {
        public Rect rect;
        public Behavior behavior;
        public bool collidable;
        public string texture;

        public MapObject(Rect rect, Behavior behavior = null, bool collidable = true, string texture = null)
        {
            this.rect = rect;
            this.behavior = behavior;
            this.collidable = collidable;
            this.texture = texture;
        }
    }

    class Map
    {
        public List<MapObject> map { get; private set; } = new List<MapObject>();
        public string name = "";

        private BinaryFormatter binaryFormatter = new BinaryFormatter();

        public void Save(string filename)
        {
            try
            {
                if (File.Exists(filename))
                    File.Delete(filename);

                FileStream stream = File.OpenWrite("content/maps/" + filename);

                binaryFormatter.Serialize(stream, map);

                stream.Close();
            }
            catch { }
        }

        public void Load(string filename)
        {
            try
            {
                FileStream stream = File.OpenRead("content/maps/" + filename);

                map = (List<MapObject>)binaryFormatter.Deserialize(stream);

                foreach (MapObject m in map) m.behavior = new DummyBehavior();

                stream.Close();
            }
            catch { }
        }

        public void Update(GameTime gameTime, Scene scene, ref Player player, bool doLogic = true)
        {
            for (int i = 0; i < map.Count; i++)
            {
                if (map[i].behavior != null)
                {
                    map[i].behavior.collision = player.Collide(map[i].rect, map[i].collidable);
                    map[i].behavior.Loop(gameTime, scene, map[i], doLogic);
                    if (!map[i].behavior.active)
                    {
                        map.RemoveAt(i);
                        i--;
                    }
                } else
                    player.Collide(map[i].rect, map[i].collidable);
            }
        }

        public void Draw(ref SpriteBatch spriteBatch)
        {
            for (int i = 0; i < map.Count; i++)
            {
                if (map[i].texture != null)
                    spriteBatch.Draw(TextureManager.textures[map[i].texture], new Rectangle(map[i].rect.X, map[i].rect.Y, map[i].rect.Width, map[i].rect.Height), Color.White);
            }
        }
    }
}