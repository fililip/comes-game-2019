﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Comes
{
    abstract class Scene
    {
        private bool firstRun = true;
        public int sceneID = -1;
        public GraphicsDeviceManager graphics;
        public SpriteBatch spriteBatch;
        public ContentManager contentManager;

        protected abstract void Start();
        
        public virtual void BeforeUpdate()
        {
            if (firstRun)
            {
                Start();
                firstRun = false;
            }
        }

        public abstract int Update(GameTime gameTime);

        protected virtual void BeforeQuit()
        {
            firstRun = true;
        }

        protected virtual void Exit()
        {
            sceneID = -1;
        }

        public abstract void Draw(ref SpriteBatch spriteBatch);
    }
}