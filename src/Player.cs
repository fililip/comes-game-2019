﻿using System.Diagnostics;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Comes
{
    class Player
    {
        private float speed = 250f;

        private Vector2 position = Vector2.Zero;
        private Vector2 size = new Vector2(32, 64);

        private Vector2 velocity = Vector2.Zero;

        private Stopwatch animationTimer = new Stopwatch();
        private bool moved = false;
        private int textureX = 0;
        private int textureY = 0;
        private int animationCounter = 0;

        public void StopInput()
        {
            velocity = new Vector2(0, 0);
        }

        public void ProcessInput(GameTime gameTime)
        {
            StopInput();

            if (Input.up || Input.upAnalog)
            {
                velocity.Y = -speed * (float)gameTime.ElapsedGameTime.TotalSeconds;
                textureX = 1;
            }

            if (Input.down || Input.downAnalog)
            {
                velocity.Y = speed * (float)gameTime.ElapsedGameTime.TotalSeconds;
                textureX = 0;
            }

            if (Input.left || Input.leftAnalog)
            {
                velocity.X = -speed * (float)gameTime.ElapsedGameTime.TotalSeconds;
                textureX = 2;
            }

            if (Input.right || Input.rightAnalog)
            {
                velocity.X = speed * (float)gameTime.ElapsedGameTime.TotalSeconds;
                textureX = 3;
            }
        }

        public Vector2 GetPosition()
        {
            return position;
        }

        public void SetPosition(Vector2 position)
        {
            this.position = position;
        }

        public Vector2 GetAnchorPosition()
        {
            Vector2 result = new Vector2(32, 32);

            bool up = Input.up || Input.upAnalog || textureX == 1;
            bool down = Input.down || Input.downAnalog || textureX == 0;
            bool left = Input.left || Input.leftAnalog || textureX == 2;
            bool right = Input.right || Input.rightAnalog || textureX == 3;

            if (up)
                result = new Vector2(32, 0);
            if (down)
                result = new Vector2(32, 64);
            if (left)
                result = new Vector2(0, 32);
            if (right)
                result = new Vector2(64, 32);
            if (up && left)
                result = new Vector2(0, 0);
            if (up && right)
                result = new Vector2(64, 0);
            if (down && left)
                result = new Vector2(0, 64);
            if (down && right)
                result = new Vector2(64, 64);

            return result + position - new Vector2(16, 0);
        }

        public Direction GetDirection()
        {
            Direction direction = Direction.Right;

            bool up = Input.up || Input.upAnalog || textureX == 1;
            bool down = Input.down || Input.downAnalog || textureX == 0;
            bool left = Input.left || Input.leftAnalog || textureX == 2;
            bool right = Input.right || Input.rightAnalog || textureX == 3;

            direction = Direction.None;

            if (up)
                direction = Direction.Up;
            if (down)
                direction = Direction.Down;
            if (left)
                direction = Direction.Left;
            if (right)
                direction = Direction.Right;
            if (up && left)
                direction = Direction.UpLeft;
            if (up && right)
                direction = Direction.UpRight;
            if (down && left)
                direction = Direction.DownLeft;
            if (down && right)
                direction = Direction.DownRight;

            return direction;
        }

        public bool Collide(Rect rect, bool collide)
        {
            if (collide)
            {
                if (velocity.X > 0 &&
                    position.X + size.X + velocity.X > rect.X &&
                    position.X < rect.X + rect.Width &&
                    position.Y + size.Y > rect.Y &&
                    position.Y < rect.Y + rect.Height)
                {
                    velocity.X = 0;
                    position.X = rect.X - size.X;
                }

                if (velocity.X < 0 &&
                    position.X + velocity.X < rect.X + rect.Width &&
                    position.X + size.X + velocity.X > rect.X &&
                    position.Y + size.Y > rect.Y &&
                    position.Y < rect.Y + rect.Height)
                {
                    velocity.X = 0;
                    position.X = rect.X + rect.Width;
                }

                if (velocity.Y > 0 &&
                    position.Y + size.Y + velocity.Y > rect.Y &&
                    position.Y < rect.Y + rect.Height &&
                    position.X + size.X > rect.X &&
                    position.X < rect.X + rect.Width)
                {
                    velocity.Y = 0;
                    position.Y = rect.Y - size.Y;
                }

                if (velocity.Y < 0 &&
                    position.Y + velocity.Y < rect.Y + rect.Height &&
                    position.Y + size.Y + velocity.Y > rect.Y &&
                    position.X + size.X > rect.X &&
                    position.X < rect.X + rect.Width)
                {
                    velocity.Y = 0;
                    position.Y = rect.Y + rect.Height;
                }
            }

            if (position.X + size.X >= rect.X &&
                position.X <= rect.X + rect.Width &&
                position.Y + size.Y >= rect.Y &&
                position.Y <= rect.Y + rect.Height)
                return true;
            else
                return false;
        }

        public void Update(GameTime gameTime)
        {
            position += velocity;

            bool moving = (velocity.X != 0 || velocity.Y != 0);

            if (moving != moved)
            {
                if (moving)
                {
                    animationTimer.Reset();
                    animationTimer.Start();
                    animationCounter = 1;
                } else
                {
                    animationTimer.Reset();
                    animationTimer.Stop();
                    animationCounter = 0;
                }

                moved = moving;
            }

            if (moving)
            {
                if (animationTimer.ElapsedMilliseconds >= 100)
                {
                    animationCounter++;

                    if (animationCounter > 3)
                        animationCounter = 0;

                    animationTimer.Restart();
                }
            }

            switch (animationCounter)
            {
                case 0:
                    textureY = 0;
                    break;
                case 1:
                    textureY = 1;
                    break;
                case 2:
                    textureY = 0;
                    break;
                case 3:
                    textureY = 2;
                    break;
            }
        }

        public void Draw(ref SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(TextureManager.textures["comes"],
                             new Rectangle(new Point((int)position.X - 16, (int)position.Y), new Point(64, 64)),
                             new Rectangle(textureX * 64, textureY * 64, 64, 64), Color.White);
        }
    }
}
