﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Comes
{
    class ComesGame : Game
    {
        public static string windowTitle = "...";
        private static string pWindowTitle = "...";

        public static int width = 1280;
        public static int height = 720;

        private static int pWidth = width;
        private static int pHeight = height;

        public static GameProperties properties = new GameProperties();

        private GraphicsDeviceManager graphics;
        private SpriteBatch spriteBatch;

        private ContentManager contentManager;

        private List<Scene> scenes = new List<Scene>();
        private int currentScene = 0;

        private Stopwatch timer = new Stopwatch();
        private string fps = "syncing...";
        private int frames = 0;

        private SpriteFont font;

        private string[] args;

        private static bool restart = false;

        public static void ShowError(string message, bool exit = false)
        {
            ConsoleColor color = Console.ForegroundColor;
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(message);
            Console.ForegroundColor = color;

            if (exit)
            {
                properties.SaveToFile("1");
                SoundManager.StopAllSounds();
                Program.Exit();
            }
        }

        public static void InitializeProperties()
        {
            if (!properties.HasProperty("name"))
                properties.SetStringProperty("name", "COMES");

            if (!properties.HasProperty("level"))
                properties.SetIntProperty("level", 1);

            if (!properties.HasProperty("hp"))
                properties.SetIntProperty("hp", 20);
        }

        public static void Restart()
        {
            restart = true;
        }

        public ComesGame(string[] args)
        {
            this.args = args;

            graphics = new GraphicsDeviceManager(this);

            properties.LoadFromFile("1");

            InitializeProperties();
        }

        protected override void Initialize()
        {
            Window.Title = "...";

            base.Initialize();

            IsMouseVisible = true;
            IsFixedTimeStep = false;

            graphics.PreferredBackBufferWidth = width;
            graphics.PreferredBackBufferHeight = height;
            graphics.SynchronizeWithVerticalRetrace = true;
            graphics.ApplyChanges();

            Window.Position = new Point(GraphicsDevice.DisplayMode.Width / 2 - graphics.PreferredBackBufferWidth / 2,
                                        GraphicsDevice.DisplayMode.Height / 2 - graphics.PreferredBackBufferHeight / 2);

            timer.Start();
        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);
            Content.RootDirectory = "content";
            contentManager = Content;

            TextureManager.Initialize(GraphicsDevice, contentManager);

            SoundManager.Initialize();

            scenes.Add(new Scenes.Shilling(ref graphics, ref spriteBatch, ref contentManager));
            scenes.Add(new Scenes.Intro(ref graphics, ref spriteBatch, ref contentManager));
            scenes.Add(new Scenes.Game(ref graphics, ref spriteBatch, ref contentManager, args));

            font = contentManager.Load<SpriteFont>("fonts/font_small");
        }

        protected override void UnloadContent()
        {
            properties.SaveToFile("1");
            SoundManager.StopAllSounds();
            Program.Exit();
        }

        protected override void Update(GameTime gameTime)
        {
            if (pWindowTitle != windowTitle)
            {
                Window.Title = windowTitle;
                pWindowTitle = windowTitle;
            }

            if (pWidth != width || pHeight != height)
            {
                graphics.PreferredBackBufferWidth = width;
                graphics.PreferredBackBufferHeight = height;
                graphics.ApplyChanges();

                pWidth = width;
                pHeight = height;
            }

            if (Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            Input.Update(this);

            if (restart)
            {
                SoundManager.StopAllSounds();

                restart = false;

                scenes.Clear();

                scenes.Add(new Scenes.Shilling(ref graphics, ref spriteBatch, ref contentManager));
                scenes.Add(new Scenes.Intro(ref graphics, ref spriteBatch, ref contentManager));
                scenes.Add(new Scenes.Game(ref graphics, ref spriteBatch, ref contentManager, args));

                GC.Collect();

                currentScene = 1;
            }

            currentScene = scenes[currentScene].Update(gameTime);

            if (currentScene < 0 || currentScene > scenes.Count - 1)
                Exit();

            SoundManager.Update();

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);

            scenes[currentScene].Draw(ref spriteBatch);

            frames++;

            if (timer.ElapsedMilliseconds >= 1000)
            {
                fps = frames.ToString() + " fps";
                frames = 0;
                timer.Restart();
            }

            spriteBatch.Begin();
            spriteBatch.DrawString(font, fps, new Vector2(width - font.MeasureString(fps).X - 8, 8), Color.White);
            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
