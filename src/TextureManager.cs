﻿using System.Collections.Generic;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Comes
{
    class TextureManager
    {
        public static Dictionary<string, Texture2D> textures { get; private set; }

        public static void Initialize(GraphicsDevice graphics, ContentManager contentManager)
        {
            textures = new Dictionary<string, Texture2D>();

            Texture2D whiteTexture = new Texture2D(graphics, 1, 1);
            whiteTexture.SetData(new Color[] { Color.White });
            whiteTexture.Name = "textures/white";

            AddTexture("white", whiteTexture);
            AddTexture("comes", contentManager.Load<Texture2D>("textures/comes"));
            AddTexture("pillow", contentManager.Load<Texture2D>("textures/pillow"));
            AddTexture("door", contentManager.Load<Texture2D>("textures/door"));

            AddTexture("sceneries/1a", contentManager.Load<Texture2D>("sceneries/1a"));
            AddTexture("sceneries/1b", contentManager.Load<Texture2D>("sceneries/1b"));
            AddTexture("sceneries/1c", contentManager.Load<Texture2D>("sceneries/1c"));
            AddTexture("sceneries/2", contentManager.Load<Texture2D>("sceneries/2"));
            AddTexture("sceneries/3", contentManager.Load<Texture2D>("sceneries/3"));
            AddTexture("sceneries/4", contentManager.Load<Texture2D>("sceneries/4"));
        }

        public static void AddTexture(string name, Texture2D texture)
        {
            textures.Add(name, texture);
        }
    }
}