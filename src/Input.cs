﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace Comes
{
    class Input
    {
        public static bool up { get; private set; } = false;
        public static bool down { get; private set; } = false;
        public static bool left { get; private set; } = false;
        public static bool right { get; private set; } = false;

        public static bool accept { get; private set; } = false;
        public static bool cancel { get; private set; } = false;
        public static bool menu { get; private set; } = false;

        public static bool dash { get; private set; } = false;
        public static bool attack { get; private set; } = false;

        public static bool upAnalog { get; private set; } = false;
        public static bool downAnalog { get; private set; } = false;
        public static bool leftAnalog { get; private set; } = false;
        public static bool rightAnalog { get; private set; } = false;

        public static bool LMB { get; private set; } = false;
        public static bool MMB { get; private set; } = false;
        public static bool RMB { get; private set; } = false;

        private static int pScrollValueMonitor = 0;
        private static int scrollValueMonitor = 0;

        public static ScrollValue scrollValue { get; private set; }

        public static Vector2 mousePosition { get; private set; } = Vector2.Zero;

        public static bool focused { get; private set; } = false;
        public static bool disableKeyboard = false;
        public static bool disableMouse = false;

        private const float deadzone = 0.1f;

        public static bool[] keys { get; private set; } = new bool[200];
        public static string textInput { get; private set; } = "";

        public static void FinishScroll()
        {
            scrollValue = 0;
        }

        public static void ResetTextInput()
        {
            textInput = "";
        }

        public enum ScrollValue
        {
            Up = 1, Down = -1
        }

        public static void Update(Game game)
        {
            focused = game.IsActive;

            if (focused)
            {
                if (disableKeyboard)
                {
                    for (int i = 0; i < keys.Length; i++)
                    {
                        bool currentPressed = Keyboard.GetState().IsKeyDown((Keys)i);

                        if (currentPressed != keys[i])
                        {
                            if (currentPressed)
                            {
                                if (i == (int)Keys.Back)
                                    if (textInput.Length > 0)
                                        textInput = textInput.Substring(0, textInput.Length - 1);

                                if (i >= (int)Keys.A && i <= (int)Keys.Z)
                                    textInput += (char)i;

                                if (keys[(int)Keys.LeftControl] && i == (int)Keys.D)
                                    ResetTextInput();
                            }

                            keys[i] = currentPressed;
                        }
                    }
                } else
                {
                    up = (GamePad.GetState(PlayerIndex.One).DPad.Up == ButtonState.Pressed ||
                          Keyboard.GetState().IsKeyDown(Keys.Up));

                    down = (GamePad.GetState(PlayerIndex.One).DPad.Down == ButtonState.Pressed ||
                            Keyboard.GetState().IsKeyDown(Keys.Down));

                    left = (GamePad.GetState(PlayerIndex.One).DPad.Left == ButtonState.Pressed ||
                            Keyboard.GetState().IsKeyDown(Keys.Left));

                    right = (GamePad.GetState(PlayerIndex.One).DPad.Right == ButtonState.Pressed ||
                             Keyboard.GetState().IsKeyDown(Keys.Right));

                    accept = (GamePad.GetState(PlayerIndex.One).Buttons.A == ButtonState.Pressed ||
                              Keyboard.GetState().IsKeyDown(Keys.Z));

                    cancel = (GamePad.GetState(PlayerIndex.One).Buttons.B == ButtonState.Pressed ||
                              Keyboard.GetState().IsKeyDown(Keys.X));

                    menu = (GamePad.GetState(PlayerIndex.One).Buttons.X == ButtonState.Pressed ||
                            Keyboard.GetState().IsKeyDown(Keys.C));

                    dash = (GamePad.GetState(PlayerIndex.One).Buttons.LeftShoulder == ButtonState.Pressed ||
                            Keyboard.GetState().IsKeyDown(Keys.LeftShift));

                    attack = (GamePad.GetState(PlayerIndex.One).Buttons.RightShoulder == ButtonState.Pressed ||
                              Keyboard.GetState().IsKeyDown(Keys.S));

                    upAnalog = GamePad.GetState(PlayerIndex.One).ThumbSticks.Left.Y > deadzone;
                    downAnalog = GamePad.GetState(PlayerIndex.One).ThumbSticks.Left.Y < -deadzone;
                    leftAnalog = GamePad.GetState(PlayerIndex.One).ThumbSticks.Left.X < -deadzone;
                    rightAnalog = GamePad.GetState(PlayerIndex.One).ThumbSticks.Left.X > deadzone;
                }

                if (!disableMouse)
                {
                    LMB = Mouse.GetState().LeftButton == ButtonState.Pressed;
                    MMB = Mouse.GetState().MiddleButton == ButtonState.Pressed;
                    RMB = Mouse.GetState().RightButton == ButtonState.Pressed;

                    mousePosition = new Vector2(Mouse.GetState().X, Mouse.GetState().Y);

                    scrollValueMonitor = Mouse.GetState().ScrollWheelValue;

                    if (pScrollValueMonitor != scrollValueMonitor)
                    {
                        if (scrollValueMonitor - pScrollValueMonitor > 0)
                            scrollValue = ScrollValue.Up;
                        else if (scrollValueMonitor - pScrollValueMonitor < 0)
                            scrollValue = ScrollValue.Down;

                        pScrollValueMonitor = scrollValueMonitor;
                    }
                }
            }
        }
    }
}
