﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Comes
{
    class Particle
    {
        public Vector2 position = Vector2.Zero;
        public Vector2 size = new Vector2(1, 1);
        public Vector2 direction = Vector2.Zero;
        public float opacity = 1f;
        public float speed = 128f;
        public int decay = 10;

        private Stopwatch decayTimer = new Stopwatch();

        private Vector2 gridPosition = Vector2.Zero;

        public bool alive { get; private set; } = true;

        public Particle()
        {
            decayTimer.Start();
        }

        public void Update(GameTime gameTime)
        {
            float delta = speed * (float)gameTime.ElapsedGameTime.TotalSeconds;

            position += direction * delta;

            gridPosition.X = ((int)(position.X / size.X)) * size.X;
            gridPosition.Y = ((int)(position.Y / size.Y)) * size.Y;

            if (decayTimer.ElapsedMilliseconds >= decay * 10)
            {
                opacity -= 0.1f;
                decayTimer.Restart();
            }

            if (opacity <= 0f)
                alive = false;
        }

        public void Draw(ref SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(TextureManager.textures["white"], new Rectangle((position - new Vector2(size.X / 2f, size.Y / 2f)).ToPoint(), size.ToPoint()), new Color(1f, 1f, 1f, opacity));
        }
    }

    class ParticleEmitter
    {
        List<Particle> particles = new List<Particle>();

        public Vector2 position = Vector2.Zero;
        public Vector2 size = Vector2.Zero;
        public int frequency = 50;

        private Stopwatch particleTimer = new Stopwatch();
        private Random random = new Random();

        public ParticleEmitter()
        {
            particleTimer.Start();
        }

        public void Update(GameTime gameTime)
        {
            if (frequency <= 0)
                frequency = 1;

            if (particleTimer.ElapsedMilliseconds >= frequency)
            {
                Particle particle = new Particle();
                particle.position = position + new Vector2(random.Next(0, (int)size.X) - size.X / 2f, random.Next(0, (int)size.Y) - size.Y / 2f);
                particle.direction = new Vector2((float)random.NextDouble() * (random.Next(0, 3) - 1), (float)random.NextDouble() * (random.Next(0, 3) - 1));

                particles.Add(particle);

                particleTimer.Restart();
            }

            for (int i = 0; i < particles.Count; i++)
            {
                Particle particle = particles[i];

                particle.Update(gameTime);

                if (!particle.alive)
                {
                    particles.Remove(particle);
                    i--;
                }
            }
        }

        public void Draw(ref SpriteBatch spriteBatch)
        {
            foreach (Particle particle in particles)
                particle.Draw(ref spriteBatch);
        }
    }
}
