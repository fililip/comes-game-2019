﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace Comes
{
    class Program
    {
        public static Stopwatch stopwatch = new Stopwatch();

        [STAThread]
        static void Main(string[] args)
        {
            DiscordRichPresence.Initialize();
            stopwatch.Start();

            foreach (string s in args)
                Console.WriteLine(s);

            string[] gameArgs = { null };

            if (args.Length > 0)
            {
                string mode = "none";

                switch (args[0])
                {
                    case "e":
                        mode = "edit";
                        break;
                    case "p":
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("level file not found");
                        Console.ForegroundColor = ConsoleColor.White;
                        break;
                    default:
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("invalid parameter");
                        Console.ForegroundColor = ConsoleColor.White;
                        break;
                }

                if (args.Length > 1)
                {
                    switch (args[0])
                    {
                        case "p":
                            gameArgs = new string[args.Length - 1];

                            for (int i = 0; i < gameArgs.Length; i++)
                                gameArgs[i] = args[i + 1];

                            ComesGame game = new ComesGame(gameArgs);
                            game.Run();

                            break;
                        default:
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine("invalid level parameter");
                            Console.ForegroundColor = ConsoleColor.White;
                            break;
                    }
                }

                switch (mode)
                {
                    case "edit":
                        Editor editor = new Editor();
                        editor.Run();
                        break;
                    default:
                    case "none":
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("invalid parameters");
                        Console.ForegroundColor = ConsoleColor.White;
                        break;
                }
            } else
            {
#if DEBUG
                Editor editor = new Editor();
                editor.Run();
#else
                ComesGame game = new ComesGame(gameArgs);
                game.Run();
#endif
            }
        }

        public static void Exit()
        {
            DiscordRichPresence.Deinitialize();
            Process.GetCurrentProcess().Kill();
        }
    }
}