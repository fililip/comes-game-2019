﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Comes
{
    class Editor : Game
    {
        public Map map = new Map();
        private Camera mapCamera = new Camera();
        private Vector2 previousCamScale = new Vector2(0.5f, 0.5f);
        private float cameraSpeed = 500f;

        private RasterizerState rasterizerState = new RasterizerState() { ScissorTestEnable = true };

        private GraphicsDeviceManager graphics;
        private SpriteBatch spriteBatch;

        private SpriteFont font;
        private UI.Canvas canvas = new UI.Canvas();

        public static readonly int width = 1280;
        public static readonly int height = 720;

        private bool pMiddleMouseButton = false;
        private bool grabbed = false;
        private Vector2 grabPoint = Vector2.Zero;

        private static int currentBlock = 0;

        private UI.Text text;
        private Vector2 mousePosition = Vector2.Zero;
        private Vector2 mouseGridPosition = Vector2.Zero;
        private Vector2 mouseGridNPosition = Vector2.Zero;

        private UI.TextBox textBox;

        private Texture2D[] textureArray;

        private CaptionSystem captionSystem;
        private UI.ScrollBox test = new UI.ScrollBox();

        public Editor()
        {
            graphics = new GraphicsDeviceManager(this);

            IsMouseVisible = true;
            IsFixedTimeStep = false;

            graphics.PreferredBackBufferWidth = width;
            graphics.PreferredBackBufferHeight = height;
            graphics.SynchronizeWithVerticalRetrace = true;
            graphics.ApplyChanges();

            Window.Position = new Point(GraphicsDevice.DisplayMode.Width / 2 - graphics.PreferredBackBufferWidth / 2,
                                        GraphicsDevice.DisplayMode.Height / 2 - graphics.PreferredBackBufferHeight / 2);
        }

        protected override void Initialize()
        {
            base.Initialize();

            mapCamera.position = new Vector2(360, 0);
            mapCamera.scale = new Vector2(0.5f, 0.5f);
            mapCamera.smooth = false;
            mapCamera.clamp = true;
        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);

            Content.RootDirectory = "content";
            TextureManager.Initialize(GraphicsDevice, Content);

            textureArray = new Texture2D[TextureManager.textures.Values.Count];
            TextureManager.textures.Values.CopyTo(textureArray, 0);

            font = Content.Load<SpriteFont>("fonts/font_smaller");

            captionSystem = new CaptionSystem(ref font);
            captionSystem.showBackground = false;
            captionSystem.position = new Vector2(width - 136, height + 8);
            captionSystem.size = 256;

            UI.Panel panel = new UI.Panel();
            panel.size = new Vector2(180, 720);
            canvas.AddElement(panel);

            text = new UI.Text(ref font);
            text.position = new Vector2(8, 8);
            canvas.AddElement(text);

            textBox = new UI.TextBox(ref font);
            textBox.color = Color.Black;
            textBox.placeholder = "textbox";
            textBox.position = new Vector2(8, 256);
            textBox.size = new Vector2(108, 16);
            canvas.AddElement(textBox);

            int index = 0;

            foreach (string s in TextureManager.textures.Keys)
            {
                Texture2D texture = TextureManager.textures[s];

                UI.Button button = new UI.Button(ref font);

                button.text = texture.Name.Split('/')[1];
                button.padding = 4;
                button.color = new Color(0.15f, 0.15f, 0.15f, 1f);
                button.position = new Vector2(8, 52 + index * 34);

                button.SetAttribute("index", index.ToString());

                if (index == 0)
                    button.color = new Color(0.2f, 0.2f, 0.2f, 1f);

                button.leftPressCallback = () =>
                {
                    int id = System.Convert.ToInt32(button.GetAttribute("index"));
                    SetIndex(id);
                };

                canvas.AddElement(button);
                index++;
            }

            canvas.AddElement(test);

            map.Load("testmap");
        }

        protected override void UnloadContent()
        {
            map.Save("testmap");
            Program.Exit();
        }

        private void SetIndex(int index)
        {
            currentBlock = index;

            for (int i = 0; i < canvas.GetElements().Length; i++)
            {
                if (canvas.GetElement(i).GetType() == typeof(UI.Button) && canvas.GetElement(i).GetAttribute("index") != null)
                {
                    if (i == index)
                        canvas.GetElement(i).color = new Color(0.2f, 0.2f, 0.2f, 1f);
                    else
                        canvas.GetElement(i).color = new Color(0.15f, 0.15f, 0.15f, 1f);
                } else
                    index++;
            }
        }

        protected override void Update(GameTime gameTime)
        {
            Input.Update(this);
            canvas.Update();
            captionSystem.Update(gameTime);

            if (Input.MMB != pMiddleMouseButton)
            {
                grabbed = Input.MMB;

                if (grabbed)
                    grabPoint = (Input.mousePosition * (1 / mapCamera.scale.X)) - mapCamera.position;

                pMiddleMouseButton = Input.MMB;
            }

            if (grabbed)
                mapCamera.position = (Input.mousePosition * (1 / mapCamera.scale.X)) - grabPoint;

            if (Input.up || Input.upAnalog)
                mapCamera.position.Y += (float)gameTime.ElapsedGameTime.TotalSeconds * cameraSpeed;

            if (Input.down || Input.downAnalog)
                mapCamera.position.Y -= (float)gameTime.ElapsedGameTime.TotalSeconds * cameraSpeed;

            if (Input.left || Input.leftAnalog)
                mapCamera.position.X += (float)gameTime.ElapsedGameTime.TotalSeconds * cameraSpeed;

            if (Input.right || Input.rightAnalog)
                mapCamera.position.X -= (float)gameTime.ElapsedGameTime.TotalSeconds * cameraSpeed;

            if (mapCamera.position.X > (180 / mapCamera.scale.X)) mapCamera.position.X = (180 / mapCamera.scale.X);
            if (mapCamera.position.Y > 0) mapCamera.position.Y = 0;

            mapCamera.position.X = (int)mapCamera.position.X;
            mapCamera.position.Y = (int)mapCamera.position.Y;

            mousePosition = new Vector2((int)(Input.mousePosition.X / mapCamera.scale.X), (int)(Input.mousePosition.Y / mapCamera.scale.Y)) - mapCamera.position;
            mouseGridPosition = new Vector2((int)(mousePosition.X / 64f), (int)(mousePosition.Y / 64f));
            mouseGridNPosition = new Vector2(mouseGridPosition.X * 64f, mouseGridPosition.Y * 64f);

            if (mouseGridPosition.X < 0)
                mouseGridPosition.X = 0;

            if (mouseGridPosition.Y < 0)
                mouseGridPosition.Y = 0;

            text.text = mousePosition.X.ToString();
            text.text += ", ";
            text.text += mousePosition.Y.ToString();

            text.text += "\n";
            text.text += "Tile: ";

            text.text += mouseGridPosition.X.ToString();
            text.text += ", ";
            text.text += mouseGridPosition.Y.ToString();

            string currentName = textureArray[currentBlock].Name.Split('/')[1];

            if (Input.mousePosition.X > 180)
            {
                bool present = false;
                int index = 0;

                if (Input.LMB || Input.RMB)
                {
                    for (int i = 0; i < map.map.Count; i++)
                    {
                        if (map.map[i].rect.X == mouseGridNPosition.X &&
                            map.map[i].rect.Y == mouseGridNPosition.Y)
                        {
                            present = true;
                            index = i;
                        }
                    }
                }

                if (Input.LMB)
                {
                    if (!present)
                    {
                        MapObject mapObject = new MapObject(new Rect((int)mouseGridNPosition.X, (int)mouseGridNPosition.Y, 64, 64),
                                                            texture: currentName);

                        map.map.Add(mapObject);
                    }
                    else
                        if (map.map[index].texture != currentName)
                            map.map[index].texture = currentName;
                }

                if (Input.RMB)
                    if (present)
                        map.map.RemoveAt(index);
                
                if (Input.scrollValue != 0)
                {
                    mapCamera.scale.X += 0.25f * (float)Input.scrollValue;

                    mapCamera.scale.X = Math.Clamp(mapCamera.scale.X, 0.25f, 2f);
                    mapCamera.scale.Y = mapCamera.scale.X;

                    if (previousCamScale != mapCamera.scale)
                    {
                        // TO DO !!
                        previousCamScale = mapCamera.scale;
                    }
                }
            }

            if (Input.menu)
            {
                /*System.Windows.Forms.DialogResult result = System.Windows.Forms.MessageBox.Show("Are you sure?", "Map deletion", System.Windows.Forms.MessageBoxButtons.YesNo);

                if (result == System.Windows.Forms.DialogResult.Yes)*/
                map.map.Clear();
            }

            mapCamera.Update(gameTime);

            Input.FinishScroll();
            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);

            spriteBatch.Begin(blendState: BlendState.NonPremultiplied, samplerState: SamplerState.PointClamp);

            canvas.Draw(ref spriteBatch);

            spriteBatch.End();

            captionSystem.Draw(ref spriteBatch);

            Rectangle rectangle = new Rectangle(180, 0, width - 180, 720);

            spriteBatch.Begin(blendState: BlendState.NonPremultiplied, rasterizerState: rasterizerState, transformMatrix: mapCamera.matrix, samplerState: SamplerState.PointClamp);
            spriteBatch.GraphicsDevice.ScissorRectangle = rectangle;

            map.Draw(ref spriteBatch);

            spriteBatch.Draw(textureArray[currentBlock], new Rectangle((int)mouseGridPosition.X * 64, (int)mouseGridPosition.Y * 64, 64, 64), new Color(1, 1, 1, 0.5f));

            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}